//
//  DetalleFotosVideosViewController.m
//  yanbal2016iphone
//
//  Created by Jeisson González on 14/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import "DetalleFotosVideosViewController.h"
#import "VisorImagenesViewController.h"

#import "UIImageView+WebCache.h"
#import "SVProgressHUD.h"

#import <QuartzCore/QuartzCore.h>


#import "HCYoutubeParser.h"
#import <QuartzCore/QuartzCore.h>
#import <MediaPlayer/MediaPlayer.h>
@import Firebase;


@interface DetalleFotosVideosViewController ()

@end

@implementation DetalleFotosVideosViewController{
    
    CGFloat scrollViewHeight;
    float paddingSection;
    float heightFoto;
    int indexImage;
}

- (void)viewDidLoad {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    [self initView];
}

-(void)initView{
    
    paddingSection=10;
    scrollViewHeight=0.0f;
    heightFoto=((IS_IPHONE_6P)?215:((IS_IPHONE_6)?195:166));
    
    
    if (!self.navigationItem.rightBarButtonItem) {
        UIImage* image = [UIImage imageNamed:@"icon_header_menu"];
        CGRect frameimg = CGRectMake(0, 0, image.size.width, image.size.height);
        UIButton *button = [[UIButton alloc] initWithFrame:frameimg];
        button.tintColor=[UIColor whiteColor];
        [button setBackgroundImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(showMenu) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    }
    
    [self pintarContenido];
    
    
    [FIRAnalytics logEventWithName:accion_fotosVideosInterna parameters:@{ @"interna": _detalle[@"titulo"] }];
    self.navigationController.navigationBar.tintColor=colorYanbal;
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Back"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self.navigationController
                                                                  action:@selector(popViewControllerAnimated:)];
    self.navigationItem.leftBarButtonItem = backButton;
}


- (void)showMenu
{
    // Dismiss keyboard (optional)
    //
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    [self.frostedViewController presentMenuViewController];
}

-(void)pintarContenido{
    
    
    [_titulo setScrollEnabled:YES];
    NSString *content=_detalle[@"titulo"];
    
    _titulo.attributedText=[self getAttrTitle:content];
    _titulo.textContainerInset = UIEdgeInsetsMake(20, 8, 0, 8);//top,left,booton,right
    [_titulo sizeToFit];
    [_titulo setFrame:CGRectMake(_titulo.frame.origin.x, scrollViewHeight, [UIScreen mainScreen].bounds.size.width,_titulo.frame.size.height)];
    _titulo.scrollEnabled = NO;
    
    
    scrollViewHeight+=_titulo.frame.size.height;
    
    _fecha.text=[_detalle[@"fecha"] componentsSeparatedByString:@" "][0];
    [_fecha setFrame:CGRectMake(_fecha.frame.origin.x, scrollViewHeight, _fecha.frame.size.width, _fecha.frame.size.height)];
    
    scrollViewHeight+=(_fecha.frame.size.height+paddingSection);
    
    
    [_contenido setScrollEnabled:YES];
    content=[_detalle[@"texto"] stringByReplacingOccurrencesOfString:@"\n\t" withString:@"<br>"];
    content=[content stringByReplacingOccurrencesOfString:@"\n" withString:@"<br>"];
    
    _contenido.attributedText=[self getAttr:content];
    _contenido.textContainerInset = UIEdgeInsetsMake(0, 8, 0, 8);//top,left,booton,right
    [_contenido sizeToFit];
    [_contenido setFrame:CGRectMake(_contenido.frame.origin.x, scrollViewHeight, [UIScreen mainScreen].bounds.size.width,_contenido.frame.size.height)];
    _contenido.scrollEnabled = NO;
    
    scrollViewHeight+=_contenido.frame.size.height;
    
    
    [_btnCompartir setFrame:CGRectMake(_btnCompartir.frame.origin.x, scrollViewHeight, _btnCompartir.frame.size.width, _btnCompartir.frame.size.height)];
    
    
    scrollViewHeight+=(_btnCompartir.frame.size.height+paddingSection+paddingSection);
    
    
    
    if (_detalle[@"galeria_imagenes"] && [_detalle[@"galeria_imagenes"] count]>0) {
        [self pintarGaleriaImagenes];
    }
    
    if (_detalle[@"galeria_videos"] && [_detalle[@"galeria_videos"] count]>0) {
        [self pintarGaleriaVideos];
    }
    
    
    [_scrollView setContentSize:(CGSizeMake(_scrollView.frame.size.width, scrollViewHeight))];

}

-(void)pintarGaleriaImagenes{
    
    int i=0;
    for (NSString *urlFoto in _detalle[@"galeria_imagenes"]) {
        UIImageView *temp=[[UIImageView alloc]initWithFrame:CGRectMake(0, scrollViewHeight, [UIScreen mainScreen].bounds.size.width, heightFoto)];
        
        temp.backgroundColor=[UIColor colorWithRed:(242/255.0) green:(242/255.0) blue:(242/255.0) alpha:1];
        
        UIActivityIndicatorView *cargando = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        CGRect frame = cargando.frame;
        frame.origin.x = temp.frame.size.width / 2 - frame.size.width / 2;
        frame.origin.y = temp.frame.size.height / 2 - frame.size.height / 2;
        
        cargando.frame = frame;
        cargando.hidesWhenStopped = YES;
        [temp addSubview:cargando];
        [cargando startAnimating];
        
        temp.contentMode=UIViewContentModeScaleAspectFill;
        [temp sd_setImageWithURL:[NSURL URLWithString:urlFoto] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            for (UIView *sub in temp.subviews) {
                if ([sub isKindOfClass:[UIActivityIndicatorView class]]) {
                    [sub removeFromSuperview];
                }
            }
            temp.clipsToBounds=YES;
            temp.image=image;
        }];
        
        temp.tag=i;i++;
        
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(galeriaTap:)];
        singleTap.numberOfTapsRequired = 1;
        singleTap.numberOfTouchesRequired = 1;
        [temp addGestureRecognizer:singleTap];
        [temp setUserInteractionEnabled:YES];
        
        
        
        [_scrollView addSubview:temp];
        scrollViewHeight+=(temp.frame.size.height+paddingSection);
    }
}


- (void)galeriaTap:(UITapGestureRecognizer *)recognizer {
    
    indexImage = (int)recognizer.view.tag;
    [self performSegueWithIdentifier:@"segMostrarGaleria" sender:self];
    
}

-(void)pintarGaleriaVideos{
    
    int i=0;
    for (NSString *codigo in _detalle[@"galeria_videos"]) {
        
        NSString *urlImgVideo=[NSString stringWithFormat:@"http://img.youtube.com/vi/%@/0.jpg",codigo];
        
        UIImageView *temp=[[UIImageView alloc]initWithFrame:CGRectMake(0, scrollViewHeight, [UIScreen mainScreen].bounds.size.width, heightFoto)];
        
        temp.backgroundColor=[UIColor colorWithRed:(242/255.0) green:(242/255.0) blue:(242/255.0) alpha:1];
        
        UIActivityIndicatorView *cargando = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        CGRect frame = cargando.frame;
        frame.origin.x = temp.frame.size.width / 2 - frame.size.width / 2;
        frame.origin.y = temp.frame.size.height / 2 - frame.size.height / 2;
        
        cargando.frame = frame;
        cargando.hidesWhenStopped = YES;
        [temp addSubview:cargando];
        [cargando startAnimating];
        
        temp.contentMode=UIViewContentModeScaleAspectFill;
        [temp sd_setImageWithURL:[NSURL URLWithString:urlImgVideo] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            for (UIView *sub in temp.subviews) {
                if ([sub isKindOfClass:[UIActivityIndicatorView class]]) {
                    [sub removeFromSuperview];
                }
            }
            temp.clipsToBounds=YES;
            temp.image=image;
            
            UIImageView *iconPlay=[[UIImageView alloc]init];
            iconPlay.image=[UIImage imageNamed:@"ic_ondemand_video_48pt"];
            CGRect framePlay = iconPlay.frame;
            framePlay.size.width=110.0f;
            framePlay.size.height=110.0f;
            framePlay.origin.x = temp.frame.size.width / 2 - framePlay.size.width / 2;
            framePlay.origin.y = temp.frame.size.height / 2 - framePlay.size.height / 2;
            
            [iconPlay setFrame:framePlay];
            [temp addSubview:iconPlay];
            
            
        }];
        
        temp.tag=i;i++;
        
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(videosTap:)];
        singleTap.numberOfTapsRequired = 1;
        singleTap.numberOfTouchesRequired = 1;
        [temp addGestureRecognizer:singleTap];
        [temp setUserInteractionEnabled:YES];
        
        
        
        [_scrollView addSubview:temp];
        scrollViewHeight+=(temp.frame.size.height+paddingSection);
    }
    
    
}


- (void)videosTap:(UITapGestureRecognizer *)recognizer {
    
    int index = (int)recognizer.view.tag;
    
    NSString *urlVideo=[NSString stringWithFormat:@"https://www.youtube.com/watch?v=%@",[_detalle[@"galeria_videos"] objectAtIndex:index]];
    
    NSDictionary *s_videos = [HCYoutubeParser h264videosWithYoutubeURL:[NSURL URLWithString: urlVideo]];
    // Presents a MoviePlayerController with the youtube quality medium
    MPMoviePlayerViewController *mp = [[MPMoviePlayerViewController alloc] initWithContentURL:[NSURL URLWithString:[s_videos objectForKey:@"medium"]]];
    [self presentMoviePlayerViewControllerAnimated:mp];
}

-(NSMutableAttributedString *)getAttrTitle:(NSString *)text{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData:[text dataUsingEncoding:NSUnicodeStringEncoding] options:@{
                                                                                                                                                             NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                                                                                             NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)
                                                                                                                                                             } documentAttributes:nil error:nil];
    
    [attributedString enumerateAttribute:NSFontAttributeName inRange:NSMakeRange(0, attributedString.length) options:NSAttributedStringEnumerationLongestEffectiveRangeNotRequired usingBlock:^(id value, NSRange range, BOOL *stop) {
        UIFont *replacementFont = nil;
        
        replacementFont = [UIFont fontWithName:@"AkzidenzGroteskBE-LightCn" size:28.0f];
        [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:range];
        [attributedString addAttribute:NSFontAttributeName value:replacementFont range:range];
        
    }];
    
    return attributedString;
}


-(NSMutableAttributedString *)getAttr:(NSString *)text{
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithData:[text dataUsingEncoding:NSUnicodeStringEncoding] options:@{
                                                                                                                                                             NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                                                                                             NSCharacterEncodingDocumentAttribute: @(NSUTF8StringEncoding)
                                                                                                                                                             } documentAttributes:nil error:nil];
    
    [attributedString enumerateAttribute:NSFontAttributeName inRange:NSMakeRange(0, attributedString.length) options:NSAttributedStringEnumerationLongestEffectiveRangeNotRequired usingBlock:^(id value, NSRange range, BOOL *stop) {
        UIFont* currentFont = value;
        UIFont *replacementFont = nil;
        
        if ([currentFont.fontName rangeOfString:@"bold" options:NSCaseInsensitiveSearch].location != NSNotFound) {
            replacementFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:14.0f];
            //[UIColor colorWithRed:213.0/255.0 green:43.0/255.0 blue:30.0/255.0 alpha:1]
            [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:range];
            [attributedString addAttribute:NSFontAttributeName value:replacementFont range:range];
            
        }else{
            replacementFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:13.0f];
            [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:range];
            [attributedString addAttribute:NSFontAttributeName value:replacementFont range:range];
        }
        
    }];
    
    return attributedString;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)compartir:(id)sender {
    
    NSString *descripcion=[NSString stringWithFormat:@"#SoySmartSoyYanbal @eventosyanbal "];
    NSURL *url=[NSURL URLWithString:_detalle[@"url"]];
    NSMutableArray *itemsToShare=[[NSMutableArray alloc]initWithObjects:descripcion,url, nil];
    [self compartirEnRedes:itemsToShare];
    
}

-(void)compartirEnRedes:(NSArray *)itemsToShare{
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:[[NSArray alloc]initWithArray:itemsToShare]
                                                                             applicationActivities:nil];
    
    activityVC.excludedActivityTypes = @[UIActivityTypePostToWeibo,
                                         UIActivityTypeMessage,
                                         UIActivityTypePrint,
                                         UIActivityTypeCopyToPasteboard,
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypeSaveToCameraRoll,
                                         UIActivityTypeAddToReadingList,
                                         UIActivityTypePostToFlickr,
                                         UIActivityTypePostToVimeo,
                                         UIActivityTypePostToTencentWeibo,
                                         UIActivityTypeAirDrop];
    [self presentViewController:activityVC animated:YES completion:nil];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    VisorImagenesViewController *temp= segue.destinationViewController;
    [temp setRecipies:_detalle[@"galeria_imagenes"]];
    [temp setIndexImage:[NSString stringWithFormat:@"%i",indexImage]];
}

@end
