//
//  FotosVideosViewController.m
//  yanbal2016iphone
//
//  Created by Jeisson González on 13/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import "FotosVideosViewController.h"
#import "DetalleFotosVideosViewController.h"

#import "UIImageView+WebCache.h"
#import "Services.h"
#import "JSNDataManager.h"

#import "CellFotosVideos.h"
@import Firebase;


@interface FotosVideosViewController ()

@end

@implementation FotosVideosViewController{
    NSMutableArray *recipes;
    NSDictionary *seleccionado;
    BOOL progress;
    int indexTab;
}

- (void)viewDidLoad {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.barTintColor = colorBlanco;
    self.navigationController.navigationBar.translucent = NO;
    
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self initView];
    
    indexTab=1;
    [self selectTab:_viewBienvenida];
    
    _table.separatorColor = [UIColor clearColor];
    _table.backgroundColor=[UIColor clearColor];
    [_table reloadData];
    
    
    [FIRAnalytics logEventWithName:accion_fotosVideos parameters:nil];
    self.navigationItem.hidesBackButton = YES;
}

-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // Navigation button was pressed. Do some stuff
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
     self.navigationItem.hidesBackButton = YES;
    [super viewWillDisappear:animated];
}

-(void)backToHome{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)showMenu
{
    // Dismiss keyboard (optional)
    //
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    [self.frostedViewController presentMenuViewController];
}

-(void)initView{
    
    
    [self addEventToViews];
    
}


-(void)addEventToViews{
    
    UITapGestureRecognizer *tapBienvenida =[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                  action:@selector(enabledTap:)];
    UITapGestureRecognizer *tapDia1 =[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                               action:@selector(enabledTap:)];
    UITapGestureRecognizer *tapDia2 =[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                             action:@selector(enabledTap:)];
    UITapGestureRecognizer *tapDia3 =[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                             action:@selector(enabledTap:)];
    
    UITapGestureRecognizer *tapDia4 =[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                             action:@selector(enabledTap:)];
    
    [_viewBienvenida addGestureRecognizer:tapBienvenida];
    [_viewDia1 addGestureRecognizer:tapDia1];
    [_viewDia2 addGestureRecognizer:tapDia2];
    [_viewDia3 addGestureRecognizer:tapDia3];
    [_viewDia4 addGestureRecognizer:tapDia4];
    
}


- (void)enabledTap:(UITapGestureRecognizer *)recognizer {
    
    int index = (int)recognizer.view.tag;
    indexTab=index;
    [self selectTab:recognizer.view];
}

-(void)initViewTab{
    
    _viewBienvenida.backgroundColor=colorGrisYanbal;
    [self setColorToLabel:_viewBienvenida color:[UIColor blackColor]];
    
    _viewDia1.backgroundColor=colorGrisYanbal;
    [self setColorToLabel:_viewDia1 color:[UIColor blackColor]];
    
    _viewDia2.backgroundColor=colorGrisYanbal;
    [self setColorToLabel:_viewDia2 color:[UIColor blackColor]];
    
    _viewDia3.backgroundColor=colorGrisYanbal;
    [self setColorToLabel:_viewDia3 color:[UIColor blackColor]];
    
    _viewDia4.backgroundColor=colorGrisYanbal;
    [self setColorToLabel:_viewDia4 color:[UIColor blackColor]];
}

-(void)setColorToLabel:(UIView *)box color:(UIColor *)color{
    
    for (UIView *subview in box.subviews) {
        if ([subview isKindOfClass:[UILabel class]]) {
            UILabel *temp=(UILabel *)subview;
            temp.textColor=color;
        }
    }
}

-(void)selectTab:(UIView *)view{
    
    [self initViewTab];
    view.backgroundColor=colorYanbal;
    [self setColorToLabel:view color:colorGrisYanbal];
    
    
    progress=NO;
    recipes=[JSNDataManager getList:[NSString stringWithFormat:@"%@%i",imagenesVideos,indexTab]];
    if (recipes.count==0) {
        progress=YES;
    }else{
        [_table reloadData];
    }
    
    [self serviceNoticias: indexTab completion:^{
        [_table reloadData];
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [recipes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *item=[recipes objectAtIndex:indexPath.row];
    
    static NSString *simpleTableIdentifier = @"CellFotosVideos";
    
    CellFotosVideos *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[CellFotosVideos alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.titulo.text=item[@"titulo"];
    cell.descripcion.text=item[@"descripcion"];
    
    NSString *urlImagen=item[@"imagen"];
    //NSLog(@"Cargando: %@",urlImagen);
    
    cell.imagen.image=nil;
    if (![urlImagen isEqualToString:@""]) {
        UIActivityIndicatorView *cargando = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        
        CGRect frame = cargando.frame;
        frame.origin.x = cell.imagen.frame.size.width / 2 - frame.size.width / 2;
        frame.origin.y = cell.imagen.frame.size.height / 2 - frame.size.height / 2;
        
        cargando.frame = frame;
        cargando.hidesWhenStopped = YES;
        [cell.imagen addSubview:cargando];
        [cargando startAnimating];
        
        [cell.imagen sd_setImageWithURL:[NSURL URLWithString:urlImagen] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            for (UIView *sub in cell.imagen.subviews) {
                if ([sub isKindOfClass:[UIActivityIndicatorView class]]) {
                    [sub removeFromSuperview];
                }
            }
            cell.imagen.image=image;
        }];
        
    }
    
    
    cell.backgroundColor=[UIColor clearColor];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (IS_IPHONE_5) {
        return 195;
    }else if(IS_IPHONE_6){
        return 235;
    }else if(IS_IPHONE_6P){
        return 252;
    }else{
        return 195;
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    seleccionado=[recipes objectAtIndex:indexPath.row];
    NSLog(@"element: %@",seleccionado);
    [self performSegueWithIdentifier:@"segDetalleFotosVideos" sender:self];
    
}


-(void)serviceNoticias:(int)index completion:(void (^)(void))completion{
    
    NSString *urlCategorias=serv_categorias;
    
    NSArray *tabs=@[@"2",@"3",@"4",@"5",@"43"];
    NSString *txtCat=[tabs objectAtIndex:index-1];
    
    urlCategorias=[urlCategorias stringByReplacingOccurrencesOfString:@"{{idCategoria}}" withString:txtCat];
    
    NSLog(@"consultando: %@",urlCategorias);
    
    _imgNoResultados.layer.opacity=0;
    
    [Services get:urlCategorias delegate:self widthProgress:progress completion:^(BOOL finished, NSDictionary *res) {
        if (finished) {
            
            NSMutableArray *tempList=[[NSMutableArray alloc]initWithArray:res[@"posts"]];
            
            NSMutableArray *tempRecipies =[[NSMutableArray alloc]init];
            
            for (NSDictionary *item in tempList) {
                NSDictionary *nuevo=@{
                                      @"url":item[@"url"],
                                      @"titulo":item[@"title"],
                                      @"fecha":((item[@"date"])?item[@"date"]:@""),
                                      @"texto":item[@"content"],
                                      @"descripcion":((item[@"custom_fields"][@"descripcion"])?item[@"custom_fields"][@"descripcion"]:@""),
                                      @"imagen":((item[@"thumbnail_images"][@"full"][@"url"])?item[@"thumbnail_images"][@"full"][@"url"]:@""),
                                      @"galeria_imagenes":((item[@"custom_fields"][@"galeria_de_imagenes"])?item[@"custom_fields"][@"galeria_de_imagenes"]:[[NSArray alloc]init]),
                                      @"galeria_videos":((item[@"custom_fields"][@"galeria_de_videos"])?item[@"custom_fields"][@"galeria_de_videos"]:[[NSArray alloc]init]),
                                      };
                [tempRecipies addObject:nuevo];
            }
            
            if (tempRecipies.count>0) {
                
                [JSNDataManager saveList:tempRecipies widthName:[NSString stringWithFormat:@"%@%i",imagenesVideos,index] callback:^(NSString *fileName) {
                    NSLog(@"Actualizado!!");
                }];
            }else{
                [JSNDataManager deleteFile:[NSString stringWithFormat:@"%@%i",imagenesVideos,index]];
                _imgNoResultados.layer.opacity=1;
            }
            
            
            
            if (indexTab==index) {
                recipes=tempRecipies;
            }
            
            completion();
        }else{
            NSLog(@"Error !!");
        }
    }];
    
}




#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    DetalleFotosVideosViewController *temp=segue.destinationViewController;
    [temp setDetalle:seleccionado];
}


@end
