//
//  HomeViewController.m
//  yanbal2016iphone
//
//  Created by Jeisson González on 11/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import "HomeViewController.h"

#import <QuartzCore/QuartzCore.h>
#import <MediaPlayer/MediaPlayer.h>
#import "Services.h"
#import "SVProgressHUD.h"
#import "JSNDataManager.h"

#import "HomeViewController.h"
#import "HorariosViewController.h"
#import "GuiaViewController.h"
#import "HotelesViewController.h"
#import "FotosVideosViewController.h"
#import "RecomendacionesViewController.h"
#import "ConcursoViewController.h"
#import "CapacitacionesViewController.h"
#import "ReporductorViewController.h"

@import Firebase;

@interface HomeViewController ()

@end

@implementation HomeViewController{
    int estado;
}

- (void)viewDidLoad {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
        for (NSString* family in [UIFont familyNames])
        {
            NSLog(@"%@", family);
    
            for (NSString* name in [UIFont fontNamesForFamilyName: family])
            {
                NSLog(@"  %@", name);
            }
        }
    
    self.view.backgroundColor=colorYanbal;
    
    estado=0;
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(verificarNotificaciones) userInfo:nil repeats:YES];
    
    
    [FIRAnalytics logEventWithName:accion_home parameters:nil];
}



/*

-(void)verificarNotificaciones{
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *notification= [defaults objectForKey:userNotification];
    
    NSDictionary *usuario= [defaults objectForKey:@"usuario"];
    
    if (notification[@"seccion"]) {
        
        
        [FIRAnalytics logEventWithName:accion_push parameters:@{ @"seccion": notification[@"seccion"] }];
        
        
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
        [SVProgressHUD showWithStatus:@"Verificando..."];
    
        dispatch_queue_t myq = dispatch_queue_create("myqueue", NULL);
        dispatch_async(myq, ^{
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                NavigationViewController *navigationController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentController"];
                HomeViewController *homeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
                
                int refresh=1;
                
                if ([notification[@"seccion"] isEqualToString:@"HORARIOS"]) {//HORARIOS
                    
                    HorariosViewController *temp = [self.storyboard instantiateViewControllerWithIdentifier:@"HorariosViewController"];
                    navigationController.viewControllers = @[homeViewController,temp];
                    
                    
                }else if([notification[@"seccion"] isEqualToString:@"GUIA"]){//GUÍA DE LOOKS
                    
                    
                    GuiaViewController *temp = [self.storyboard instantiateViewControllerWithIdentifier:@"GuiaViewController"];
                    navigationController.viewControllers = @[homeViewController,temp];
                    
                }else if([notification[@"seccion"] isEqualToString:@"HOTELES"]){//HOTELES
                    
                    
                    HotelesViewController *temp = [self.storyboard instantiateViewControllerWithIdentifier:@"HotelesViewController"];
                    navigationController.viewControllers = @[homeViewController,temp];
                    
                    [defaults removeObjectForKey:userNotification];
                    [defaults synchronize];
                    
                    
                }else if([notification[@"seccion"] isEqualToString:@"FOTOSYVIDEOS"]){//FOTOS & VIDEOS
                    
                    
                    FotosVideosViewController *temp = [self.storyboard instantiateViewControllerWithIdentifier:@"FotosVideosViewController"];
                    navigationController.viewControllers = @[homeViewController,temp];
                    
                    [defaults removeObjectForKey:userNotification];
                    [defaults synchronize];
                    
                    
                }else if([notification[@"seccion"] isEqualToString:@"RECOMENDACIONES"]){//RECOMENDACIONES
                    
                    RecomendacionesViewController *temp = [self.storyboard instantiateViewControllerWithIdentifier:@"RecomendacionesViewController"];
                    navigationController.viewControllers = @[homeViewController,temp];
                    
                    
                }else if([notification[@"seccion"] isEqualToString:@"CONCURSO"]){//PARTICIPA #50ANIVERSARIOYANBAL
                    
                    ConcursoViewController *temp = [self.storyboard instantiateViewControllerWithIdentifier:@"ConcursoViewController"];
                    navigationController.viewControllers = @[homeViewController,temp];
                    
                    [defaults removeObjectForKey:userNotification];
                    [defaults synchronize];
                    
                }else if([notification[@"seccion"] isEqualToString:@"CAPACITACIONES"]){//CAPACITACIONES
                    
                    refresh=0;
                    if (usuario[@"username"]) {
                        [self verCapacitaciones];
                    }else{
                        estado=1;
                        [self showLogin];
                    }
                    
                    [defaults removeObjectForKey:userNotification];
                    [defaults synchronize];
                    
                }else if([notification[@"seccion"] isEqualToString:@"STREAMING"]){//TRANSMISIÓN EN VIVO
                    
                    refresh=0;
                    if (usuario[@"username"]) {
                        [self verStreaming];
                    }else{
                        estado=2;
                        [self showLogin];
                    }
                    
                    [defaults removeObjectForKey:userNotification];
                    [defaults synchronize];
                    
                }else{
                    [defaults removeObjectForKey:userNotification];
                    [defaults synchronize];
                }
                
                if (refresh==1) {
                    self.frostedViewController.contentViewController = navigationController;
                    [self.frostedViewController hideMenuViewController];
                }
                
                [SVProgressHUD dismiss];
                
            });
        });
        
    }
}*/





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)loguin:(id)sender {
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *usuario= [defaults objectForKey:@"usuario"];
    if (usuario[@"username"]) {
        [self verCapacitaciones];
    }else{
        estado=1;
        [self showLogin];
    }
}


- (IBAction)moduloCamara:(id)sender {
    
    [self performSegueWithIdentifier:@"seg_camara" sender:self];
}


-(void)showLogin{
    [self performSegueWithIdentifier:@"segLoguin" sender:self];

}



-(void)verCapacitaciones{
    
    [self performSegueWithIdentifier:@"segCapacitaciones" sender:self];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
}


@end
