//
//  DetalleHotelViewController.h
//  yanbal2016iphone
//
//  Created by Jeisson González on 12/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"
#import "PagedFlowView.h"

@interface DetalleHotelViewController : UIViewController<PagedFlowViewDelegate,PagedFlowViewDataSource>

@property (strong, nonatomic) IBOutlet NSDictionary *detalle;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIImageView *imagen;
@property (strong, nonatomic) IBOutlet UIButton *btnHotel;

@property (strong, nonatomic) IBOutlet PagedFlowView *hFlowView;

@property (nonatomic, strong) IBOutlet UIPageControl *hPageControl;
- (IBAction)pageControlValueDidChange:(id)sender;
@end
