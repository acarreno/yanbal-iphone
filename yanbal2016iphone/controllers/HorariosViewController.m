//
//  HorariosViewController.m
//  yanbal2016iphone
//
//  Created by Jeisson González on 11/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import "HorariosViewController.h"

#import "UIImageView+WebCache.h"
#import "Services.h"
#import "JSNDataManager.h"
@import Firebase;

@interface HorariosViewController ()

@end

@implementation HorariosViewController{
    NSMutableArray *horarios;
    BOOL progress;
    int indexTab;
}

- (void)viewDidLoad {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.barTintColor = colorBlanco;
    self.navigationController.navigationBar.translucent = NO;
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self initView];
    
    horarios=[JSNDataManager getList:fileHorarios];
    
    progress=NO;
    if (horarios.count==0) {
        progress=YES;
    }else{
        indexTab=1;
        [self selectTab:_viewMiercoles];
        [self verificarNotificacion];
    }
    
    [self serviceHorarios];
    
    [FIRAnalytics logEventWithName:accion_horarios parameters:nil];
    self.navigationItem.hidesBackButton = YES;
}

-(void)verificarNotificacion{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *notification= [defaults objectForKey:userNotification];
    
    if (notification[@"seccion"]) {
        if ([notification[@"seccion"] isEqualToString:@"HORARIOS"]) {
            
            indexTab=[notification[@"interna"] intValue];
            
            switch (indexTab) {
                case 1:
                    [self selectTab:_viewMiercoles];
                    break;
                case 2:
                    [self selectTab:_viewJueves];
                    break;
                case 3:
                    [self selectTab:_viewViernes];
                    break;
                case 4:
                    [self selectTab:_viewSabado];
                    break;
                    
                default:
                    break;
            }
            
            [defaults removeObjectForKey:userNotification];
            [defaults synchronize];
        }
    }
}

-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // Navigation button was pressed. Do some stuff
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    [super viewWillDisappear:animated];
}

-(void)backToHome{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)showMenu
{
    // Dismiss keyboard (optional)
    //
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    [self.frostedViewController presentMenuViewController];
}

-(void)initView{
    
    
    
    [self addEventToViews];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)addEventToViews{
    
    
    UITapGestureRecognizer *tapMiercoles =[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                  action:@selector(enabledTap:)];
    UITapGestureRecognizer *tapJueves =[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                               action:@selector(enabledTap:)];
    UITapGestureRecognizer *tapViernes =[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(enabledTap:)];
    
    
    UITapGestureRecognizer *tapSabado =[[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(enabledTap:)];
    
    [_viewMiercoles addGestureRecognizer:tapMiercoles];
    [_viewJueves addGestureRecognizer:tapJueves];
    [_viewViernes addGestureRecognizer:tapViernes];
    [_viewSabado addGestureRecognizer:tapSabado];
    
}


- (void)enabledTap:(UITapGestureRecognizer *)recognizer {
    
    int index = (int)recognizer.view.tag;
    indexTab=index;
    [self selectTab:recognizer.view];
    
    
}

-(void)initViewTab{
    
    _viewMiercoles.backgroundColor=colorGrisYanbal;
    [self setColorToLabel:_viewMiercoles color:[UIColor blackColor]];
    
    _viewJueves.backgroundColor=colorGrisYanbal;
    [self setColorToLabel:_viewJueves color:[UIColor blackColor]];
    
    _viewViernes.backgroundColor=colorGrisYanbal;
    [self setColorToLabel:_viewViernes color:[UIColor blackColor]];
    
    
    _viewSabado.backgroundColor=colorGrisYanbal;
    [self setColorToLabel:_viewSabado color:[UIColor blackColor]];
}


-(void)setColorToLabel:(UIView *)box color:(UIColor *)color{
    
    for (UIView *subview in box.subviews) {
        if ([subview isKindOfClass:[UILabel class]]) {
            UILabel *temp=(UILabel *)subview;
            temp.textColor=color;
        }
    }
}

-(void)selectTab:(UIView *)view{
    
    
    [self initViewTab];
    view.backgroundColor=colorYanbal;
    [self setColorToLabel:view color:colorGrisYanbal];
    
    for (UIView *subView in _imagen.subviews) {
        [subView removeFromSuperview];
    }
    
    NSArray *tabs=@[@"Domingo",@"Lunes",@"Martes"];
    NSString *txtDia=[tabs objectAtIndex:indexTab-1];
    
    
    for (NSDictionary *dia in horarios) {
        if ([dia[@"dia"] isEqualToString:txtDia]) {
            
            UIActivityIndicatorView *cargando = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            
            CGRect frame = cargando.frame;
            frame.origin.x = _imagen.frame.size.width / 2 - frame.size.width / 2;
            frame.origin.y = _imagen.frame.size.height / 2 - frame.size.height / 2;
            
            cargando.frame = frame;
            cargando.hidesWhenStopped = YES;
            [_imagen addSubview:cargando];
            [cargando startAnimating];
            
            
            NSString *urlImagen=dia[@"urlImagen"];
            NSLog(@"Cargando: %@",urlImagen);
            
            if (![urlImagen isEqualToString:@""]) {
                
                [_imagen sd_setImageWithURL:[NSURL URLWithString:urlImagen] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
                    
                    for (UIView *sub in _imagen.subviews) {
                        if ([sub isKindOfClass:[UIActivityIndicatorView class]]) {
                            [sub removeFromSuperview];
                        }
                    }
                    
                    _imagen.image=image;
                    
                    //1440 -> 18809
                    //320 ->
                    
                    float heigth= (320 * image.size.height) / image.size.width;
                    
                    CGRect myImageRect = CGRectMake(0.0f, 0.0f, _scrollView.frame.size.width, heigth);
                    _imagen.frame=myImageRect;
                    [_scrollView setContentSize:(CGSizeMake(_scrollView.frame.size.width, heigth))];
                }];
                
            }
        }
    }
    
}

-(void)serviceHorarios{

    [Services get:serv_horarios delegate:self widthProgress:progress completion:^(BOOL finished, NSDictionary *res) {
        if (finished && [res[@"posts"] count]>0) {
            
            NSMutableArray *tempList=[[NSMutableArray alloc]initWithArray:res[@"posts"]];
            
            horarios=[[NSMutableArray alloc]init];
            for (NSDictionary *item in tempList) {
                NSDictionary *nuevo=@{
                                      @"dia":item[@"title"],
                                      @"urlImagen":item[@"thumbnail_images"][@"full"][@"url"]
                                      };
                [horarios addObject:nuevo];
            }
            
            [JSNDataManager saveList:horarios widthName:fileHorarios callback:^(NSString *fileName) {
                NSLog(@"Actualizado!!");
            }];
            
            if (progress) {
                indexTab=1;
                [self selectTab:_viewMiercoles];
                [self verificarNotificacion];
            }
           
            
        }else{
            NSLog(@"Error !!");
        }
    }];
    
}



@end
