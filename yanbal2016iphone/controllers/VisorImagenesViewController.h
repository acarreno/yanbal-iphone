//
//  VisorImagenesViewController.h
//  yanbal2016iphone
//
//  Created by Jeisson González on 18/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PagedFlowView.h"

@interface VisorImagenesViewController : UIViewController<PagedFlowViewDelegate,PagedFlowViewDataSource>

@property (strong, nonatomic) IBOutlet PagedFlowView *hFlowView;
@property (nonatomic, strong) IBOutlet UIPageControl *hPageControl;
- (IBAction)pageControlValueDidChange:(id)sender;
@property (strong, nonatomic)  NSMutableArray *recipies;
@property (strong, nonatomic)  NSString *indexImage;


@end
