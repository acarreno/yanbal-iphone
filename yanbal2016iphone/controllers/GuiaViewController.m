//
//  GuiaViewController.m
//  yanbal2016iphone
//
//  Created by Jeisson González on 12/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import "GuiaViewController.h"
@import Firebase;

@interface GuiaViewController ()

@end

@implementation GuiaViewController{
    NSMutableArray *recipies;
}

- (void)viewDidLoad {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.barTintColor = colorBlanco;
    self.navigationController.navigationBar.translucent = NO;
    
    
    recipies=[[NSMutableArray alloc]init];
    
    for (int i=1; i<9; i++) {
        [recipies addObject:[NSString stringWithFormat:@"img_guiadelook_%i",i]];
    }
    
    
    _hFlowView.delegate = self;
    _hFlowView.dataSource = self;
    _hFlowView.pageControl = _hPageControl;
    _hFlowView.minimumPageAlpha = 1;
    _hFlowView.minimumPageScale = 1;
    _hFlowView.orientation = PagedFlowViewOrientationHorizontal;
    [_hFlowView reloadData];
    
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self initView];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [self verificarNotificacion];
}

-(void)verificarNotificacion{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *notification= [defaults objectForKey:userNotification];
    
    if (notification[@"seccion"]) {
        if ([notification[@"seccion"] isEqualToString:@"GUIA"]) {
            
            int index=[notification[@"interna"] intValue];
            
            _hPageControl.currentPage=index;
            [_hFlowView scrollToPage:index];
            
            [defaults removeObjectForKey:userNotification];
            [defaults synchronize];
        }
    }
}

-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // Navigation button was pressed. Do some stuff
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    [super viewWillDisappear:animated];
}

-(void)backToHome{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)showMenu
{
    // Dismiss keyboard (optional)
    //
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    [self.frostedViewController presentMenuViewController];
}

-(void)initView{
    
    [FIRAnalytics logEventWithName:accion_guia parameters:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark PagedFlowView Delegate
- (CGSize)sizeForPageInFlowView:(PagedFlowView *)flowView;{
    return CGSizeMake(_hFlowView.frame.size.width, _hFlowView.frame.size.height);
}

- (void)flowView:(PagedFlowView *)flowView didScrollToPageAtIndex:(NSInteger)index {
    NSLog(@"Scrolled to page # %ld", (long)index);
}

- (void)flowView:(PagedFlowView *)flowView didTapPageAtIndex:(NSInteger)index{
    NSLog(@"Scrolled to page # %ld", (long)index);
    
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark PagedFlowView Datasource
//返回显示View的个数
- (NSInteger)numberOfPagesInFlowView:(PagedFlowView *)flowView{
    return [recipies count];
}

//返回给某列使用的View
- (UIView *)flowView:(PagedFlowView *)flowView cellForPageAtIndex:(NSInteger)index{
    UIImageView *imageView = (UIImageView *)[flowView dequeueReusableCell];
    if (!imageView) {
        imageView = [[UIImageView alloc] init];
    }
    
    NSString *item=[recipies objectAtIndex:index];
    
    imageView.contentMode=UIViewContentModeScaleAspectFit;
    imageView.clipsToBounds = NO;
    imageView.backgroundColor=[UIColor whiteColor];
    [imageView setFrame:CGRectMake(0, 0, _hFlowView.frame.size.width, _hFlowView.frame.size.height)];
    imageView.image=[UIImage imageNamed:item];
    
    return imageView;
}

- (IBAction)pageControlValueDidChange:(id)sender {
    UIPageControl *pageControl = sender;
    [_hFlowView scrollToPage:pageControl.currentPage];
}

@end
