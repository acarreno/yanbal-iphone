//
//  TabBarViewController.m
//  yanbal2016iphone
//
//  Created by IMAC on 28/02/18.
//  Copyright © 2018 wigilabs. All rights reserved.
//

#import "TabBarViewController.h"
#import "MenuTabBarViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "PulsingHaloLayer.h"
@import Firebase;

@interface TabBarViewController ()

@end

@implementation TabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initView];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self verificarEnvivo];
}

-(void)initView{
    
    [FIRAnalytics logEventWithName:accion_home parameters:nil];
    [FIRAnalytics logEventWithName:btnTag_inicio parameters:nil];
    
    self.delegate = self;
    
    
    [self.viewControllers enumerateObjectsUsingBlock:^(UIViewController *vc, NSUInteger idx, BOOL *stop) {
        vc.tabBarItem.title = nil;
        vc.tabBarItem.imageInsets = UIEdgeInsetsMake(6, 0, -6, 0);
        
    }];
    
    
    [self.tabBar.items enumerateObjectsUsingBlock:^(UITabBarItem *item, NSUInteger idx, BOOL *stop) {
        UITabBarItem *btn=item;
        btn.image=[self tabBarImageWithCustomTint:[UIColor whiteColor] image:btn.image];
        
    }];
    
    float distance=75;
    
    if (IS_IPHONE_X){
        distance=109;
    }
    
    
    UIView *view=[[UIView alloc]initWithFrame:CGRectMake(SCREEN_WIDTH/2-35, SCREEN_HEIGHT-distance, 70, 70)];
    view.backgroundColor=[UIColor whiteColor];
    UIImageView *imageView=[[UIImageView alloc]initWithFrame:CGRectMake(view.frame.size.width/2-20, view.frame.size.height/2-16, 40, 33)];
    imageView.image=[UIImage imageNamed:@"icon_camara"];
    imageView.contentMode=UIViewContentModeScaleAspectFit;
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(openCamera)];
    singleTap.numberOfTapsRequired = 1;
    [imageView setUserInteractionEnabled:YES];
    [imageView addGestureRecognizer:singleTap];
    [view addSubview:imageView];
    view.layer.cornerRadius=view.frame.size.height/2;
    
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(-1, 1);
    view.layer.shadowRadius = 5;
    view.layer.shadowOpacity = 0.5;
    
    
    [self.view addSubview:view];
    
    self.tabBar.barTintColor=colorYanbal;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(verControlador:)
                                                 name:noti_ver_controlador
                                               object:nil];
    
}
-(void)openCamera{
    
    dispatch_queue_t myq = dispatch_queue_create("myqueue", NULL);
    dispatch_async(myq, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            
            BackCameraController *newController = [[BackCameraController alloc]init];
            [newController InitPluginWithParent:self marco:@"marco1"];
            
        });
    });
    [FIRAnalytics logEventWithName:accion_camara parameters:nil];
    
}

- (UIImage *)tabBarImageWithCustomTint:(UIColor *)tintColor image:(UIImage *)image
{
    UIGraphicsBeginImageContextWithOptions(image.size, NO, image.scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, 0, image.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    CGContextSetBlendMode(context, kCGBlendModeNormal);
    CGRect rect = CGRectMake(0, 0, image.size.width, image.size.height);
    CGContextClipToMask(context, rect, image.CGImage);
    [tintColor setFill];
    CGContextFillRect(context, rect);
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    newImage = [newImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    return newImage;
}

- (BOOL) tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController
{
    BOOL acceso=YES;
    
    if(viewController == [tabBarController.viewControllers objectAtIndex:0]){//Home
        
    }else if(viewController == [tabBarController.viewControllers objectAtIndex:1]){//guia de looks
        
    }else if(viewController == [tabBarController.viewControllers objectAtIndex:2]){//camara
        acceso=NO;
       
    }else if(viewController == [tabBarController.viewControllers objectAtIndex:3]){//horarios
        
    }else if(viewController == [tabBarController.viewControllers objectAtIndex:4]){//mas
        acceso=NO;
        [self mostrarMenu];
    }
    
    return acceso;
}

-(void)mostrarMenu{
    
    UIStoryboard *appStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    MenuTabBarViewController *temp=[appStoryboard instantiateViewControllerWithIdentifier:@"MenuTabBar"];
    temp.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    temp.providesPresentationContextTransitionStyle = YES;
    temp.definesPresentationContext = YES;
    [temp setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self presentViewController:temp animated:YES completion:nil];
    });
    [FIRAnalytics logEventWithName:btnTag_mas parameters:nil];
}

-(void)verControlador:(NSNotification *)notification{
    NSDictionary *item=notification.object;
    [self setSelectedIndex:2];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:item forKey:@"modulo"];
    [defaults synchronize];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:noti_set_controlador object:item];
    });
    
}

-(void)verificarEnvivo{
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    int year = (int)[components year];
    int month = (int)[components month];
    int day = (int)[components day];
    //53x53 btn_envivo
    NSLog(@"Hoy es: el %i del mes %i de %i",day,month,year);
    
    if (day==9 && month == 4) {//Es la hora del evento en vivo
        
        int margen=200;
        if (IS_IPHONE_5) {
            margen=100;
        }
        
        UIView *view=[[UIView alloc]initWithFrame:CGRectMake(4, SCREEN_HEIGHT/2 - margen, 53, 53)];
        UIButton *boton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 53, 53)];
        [boton setImage:[UIImage imageNamed:@"btn_envivo"] forState:UIControlStateNormal];
        [boton addTarget:self
                     action:@selector(verStreaming)
           forControlEvents:UIControlEventTouchUpInside];
        
        PulsingHaloLayer *halo = [PulsingHaloLayer layer];
        UIColor *color = colorYanbal;
        halo.backgroundColor = color.CGColor;
        halo.haloLayerNumber = 3;
        halo.position = boton.center;
        [view.layer addSublayer:halo];
        [halo start];
        
        [view addSubview:boton];
        
        view.layer.opacity=0;
        [self.view addSubview:view];
        
        [UIView animateWithDuration:.5 animations:^{
            view.layer.opacity=1;
        }];
        
    }
}

-(void)verStreaming{
    NSLog(@"Clic en el streaming... ");
    
    
    UIStoryboard *appStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *temp=[appStoryboard instantiateViewControllerWithIdentifier:@"navLoginViewController"];
    [self presentViewController:temp animated:YES completion:nil];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
