//
//  ReporductorViewController.m
//  yanbal2016iphone
//
//  Created by Jeisson González on 18/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import "ReporductorViewController.h"

@import Firebase;

@interface ReporductorViewController ()

@end

@implementation ReporductorViewController

- (void)viewDidLoad {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.barTintColor = colorBlanco;
    self.navigationController.navigationBar.translucent = NO;
    
    NSLog(@"data: %@",_data);
    
    NSString *url = [NSString stringWithFormat:@"%@",_data[@"custom_fields"][@"descripcion"]];
    NSURL *nsUrl = [NSURL URLWithString:url];
    NSURLRequest *request = [NSURLRequest requestWithURL:nsUrl cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:30];
    _navegador.backgroundColor=[UIColor blackColor];
    
    UIActivityIndicatorView *cargando = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    
    
    CGRect frame = _navegador.frame;
    frame.origin.x = [UIScreen mainScreen].bounds.size.width / 2 - frame.size.width / 2;
    frame.origin.y = [UIScreen mainScreen].bounds.size.height / 2 - frame.size.height / 2;
    
    cargando.frame = frame;
    cargando.hidesWhenStopped = YES;
    [self.view addSubview:cargando];
    [cargando startAnimating];
    
    _navegador.delegate = self;
    [_navegador loadRequest:request];
    
    
    [self.navigationController setNavigationBarHidden:YES];
    [FIRAnalytics logEventWithName:accion_streaming parameters:nil];
    self.navigationItem.hidesBackButton = YES;
}

// In the UIWebViewDelegate
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSLog(@"url: %@",request);
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    for (UIView *subView in self.view.subviews) {
        if ([subView isKindOfClass:[UIActivityIndicatorView class]]) {
            [subView removeFromSuperview];
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)salir:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
