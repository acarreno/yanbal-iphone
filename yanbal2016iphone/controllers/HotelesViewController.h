//
//  HotelesViewController.h
//  yanbal2016iphone
//
//  Created by Jeisson González on 12/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"

@interface HotelesViewController : UIViewController<UICollectionViewDelegate, UICollectionViewDataSource>


//@property (strong, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) IBOutlet UICollectionView *table;
@property (strong, nonatomic) IBOutlet NSMutableArray *recipes;

@end
