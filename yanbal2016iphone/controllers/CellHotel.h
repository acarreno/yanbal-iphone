//
//  CellHotel.h
//  yanbal2016iphone
//
//  Created by Jeisson González on 12/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellHotel : UICollectionViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imgHotel;
@end

