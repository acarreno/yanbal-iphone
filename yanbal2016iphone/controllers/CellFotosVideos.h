//
//  CellFotosVideos.h
//  yanbal2016iphone
//
//  Created by Jeisson González on 13/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellFotosVideos : UITableViewCell

@property (strong, nonatomic) IBOutlet UIImageView *imagen;
@property (strong, nonatomic) IBOutlet UILabel *titulo;
@property (strong, nonatomic) IBOutlet UILabel *descripcion;
@end
