//
//  TabBarViewController.h
//  yanbal2016iphone
//
//  Created by IMAC on 28/02/18.
//  Copyright © 2018 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <yanbal2016iphone-Swift.h>
@interface TabBarViewController : UITabBarController<UITabBarControllerDelegate>

@end
