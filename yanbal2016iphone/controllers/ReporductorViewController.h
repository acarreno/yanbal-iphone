//
//  ReporductorViewController.h
//  yanbal2016iphone
//
//  Created by Jeisson González on 18/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ReporductorViewController : UIViewController<UIWebViewDelegate>
@property (strong, nonatomic) NSDictionary *data;
@property (strong, nonatomic) IBOutlet UIWebView *navegador;
@end
