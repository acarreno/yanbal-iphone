//
//  MenuTabBarViewController.m
//  yanbal2016iphone
//
//  Created by IMAC on 1/03/18.
//  Copyright © 2018 wigilabs. All rights reserved.
//

#import "MenuTabBarViewController.h"
@import Firebase;
@interface MenuTabBarViewController ()

@end

@implementation MenuTabBarViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _viewContainer.layer.opacity=0;
    
    if (!UIAccessibilityIsReduceTransparencyEnabled()) {
        _viewBlur.backgroundColor = [UIColor clearColor];
        
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
        UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        //always fill the view
        blurEffectView.frame = _viewBlur.bounds;
        blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [_viewBlur addSubview:blurEffectView]; //if you have more UIViews, use an insertSubview API to place it where needed
    } else {
        _viewBlur.backgroundColor = [UIColor grayColor];
    }
    
    
    [UIView animateWithDuration:.5 animations:^{
        _viewContainer.layer.opacity=1;
    }];
    [FIRAnalytics logEventWithName:btnTag_mas parameters:nil];
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cerrar:(id)sender {
    [FIRAnalytics logEventWithName:btnTag_cerrar parameters:nil];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)actionHoteles:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:noti_ver_controlador object:@{@"controller":@"HotelesViewController"}];
}

- (IBAction)actionRecomendaciones:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:noti_ver_controlador object:@{@"controller":@"RecomendacionesViewController"}];
}

- (IBAction)actionFotosVideos:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:noti_ver_controlador object:@{@"controller":@"FotosVideosViewController"}];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
