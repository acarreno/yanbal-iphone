//
//  MenuTabBarViewController.h
//  yanbal2016iphone
//
//  Created by IMAC on 1/03/18.
//  Copyright © 2018 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TabBarViewController.h"

@interface MenuTabBarViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet UIView *viewBlur;
@end
