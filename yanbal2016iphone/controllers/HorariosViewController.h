//
//  HorariosViewController.h
//  yanbal2016iphone
//
//  Created by Jeisson González on 11/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"

@interface HorariosViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *viewMiercoles;
@property (strong, nonatomic) IBOutlet UIView *viewJueves;
@property (strong, nonatomic) IBOutlet UIView *viewViernes;
@property (strong, nonatomic) IBOutlet UIView *viewSabado;
@property (strong, nonatomic) IBOutlet UIImageView *imagen;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;

@end
