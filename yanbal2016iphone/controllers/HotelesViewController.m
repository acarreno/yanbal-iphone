//
//  HotelesViewController.m
//  yanbal2016ipad
//
//  Created by Jeisson González on 22/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import "HotelesViewController.h"
#import "DetalleHotelViewController.h"
#import "CellHotel.h"
@import Firebase;

@interface HotelesViewController ()

@end

@implementation HotelesViewController{
    NSDictionary *seleccionado;
    NSMutableArray *list;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController setNavigationBarHidden:NO];
    [FIRAnalytics logEventWithName:accion_hoteles parameters:nil];
    
    list= [[NSMutableArray alloc]initWithObjects:
           @{
             @"id":@3,
             @"nombre":@"Torre del mar",
             @"imagen":@"htl_torredelmar",
             @"galeria":@[
                     @"Torredelmar_1",
                     @"Torredelmar_2",
                     @"Torredelmar_3",
                     @"Torredelmar_4"
                     
                     ],
             @"interna":@"americas torredelmar",
             @"url":@"http://www.hotellasamericas.com.co/?gclid=Cj0KCQjwkKPVBRDtARIsAA2CG6FbxEy9hy1vO0-cHPOF7gZZGbdzA0FZvzegd5oROk258RMovzFe29gaAsFSEALw_wcB"
             },
           @{
             @"id":@0,
             @"nombre":@"Casa de playa",
             @"imagen":@"htl_casadeplaya",
             @"galeria":@[
                     @"casadeplaya_1",
                     @"casadeplaya_2",
                     @"casadeplaya_3",
                     @"casadeplaya_4"
                     ],
             @"interna":@"americascasadeplaya",
             @"url":@"http://www.hotellasamericas.com.co/?gclid=Cj0KCQjwkKPVBRDtARIsAA2CG6FbxEy9hy1vO0-cHPOF7gZZGbdzA0FZvzegd5oROk258RMovzFe29gaAsFSEALw_wcB"
             },
           @{
             @"id":@1,
             @"nombre":@"Hollyday Inn",
             @"imagen":@"htl_hollidayinn",
             @"galeria":@[
                     @"Hollidayinn_1",
                     @"Hollidayinn_2",
                     @"Hollidayinn_3",
                     @"Hollidayinn_4"
                     
                     ],
             @"interna":@"holiday",
             @"url":@"https://www.holidayinn.com/hotels/us/es/reservation?qAdlt=1&qBrs=6c.hi.ex.rs.ic.cp.in.sb.cw.cv.ul.vn.ki.sp.nd.ct&qChld=0&qFRA=1&qGRM=0&qIta=99628911&qPSt=0&qRRSrt=rt&qRef=df&qRms=1&qRpn=1&qRpp=20&qSHp=1&qSmP=3&qSrt=sBR&qWch=0&srb_u=1&icdv=99628911&cid=101942&dp=true&glat=SEAR&setPMCookies=true"
             },
           @{
             @"id":@2,
             @"nombre":@"Radisson",
             @"imagen":@"htl_radisson",
             @"galeria":@[
                     @"Radisson_1",
                     @"Radisson_2",
                     @"Radisson_3",
                     @"Radisson_4"
                     
                     ],
             @"interna":@"radisson",
             @"url":@"https://www.radisson.com/"
             },
           
           nil];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    NSLog(@"view will Appear");
   self.navigationItem.hidesBackButton = YES;
}
#pragma mark - collectionView

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return list.count;
}


- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSDictionary *item=[list objectAtIndex:indexPath.row];
    static NSString *identifier=@"Cell";
    
    CellHotel *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.imgHotel.image=[UIImage imageNamed:item[@"imagen"]];
    
    
    return cell;
}


-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSDictionary *item=[list objectAtIndex:indexPath.row];
    
    UIImage *image=[UIImage imageNamed:item[@"imagen"]];
    float with=(self.table.frame.size.width/2)-5;
    float heigth=(with*image.size.height)/image.size.width;
    
    return CGSizeMake(with, heigth);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    seleccionado=[list objectAtIndex:indexPath.row];
    
    [self performSegueWithIdentifier:@"segDetalleHoteles" sender:self];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
   DetalleHotelViewController *temp= segue.destinationViewController;
    [temp setDetalle:seleccionado];
    
}


-(float)getsize:(int)value{
    
    return (value*SCREEN_WIDTH)/768;
    
}

@end

