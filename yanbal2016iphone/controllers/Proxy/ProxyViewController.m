//
//  ProxyViewController.m
//  yanbal2016iphone
//
//  Created by IMAC on 1/03/18.
//  Copyright © 2018 wigilabs. All rights reserved.
//

#import "ProxyViewController.h"
#import "HotelesViewController.h"
#import "RecomendacionesViewController.h"
#import "FotosVideosViewController.h"

@interface ProxyViewController ()

@end

@implementation ProxyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initView];
}

-(void)initView{
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(verControlador:)
                                                 name:noti_set_controlador
                                               object:nil];
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *item=[defaults objectForKey:@"modulo"];
    
    if (item) {
        [defaults removeObjectForKey:@"modulo"];
        [defaults synchronize];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:noti_set_controlador object:item];
        });
    }
}

-(void)verControlador:(NSNotification *)notification{
    NSDictionary *item=notification.object;
    NSLog(@"agregar: %@",item);
    
    for (UIViewController *temp in self.navigationController.childViewControllers) {
        if (![temp isKindOfClass:[self class]]) {
            [temp removeFromParentViewController];
            [temp.view removeFromSuperview];
        }
    }
    
    UIStoryboard *appStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    
    if ([item[@"controller"] isEqualToString:@"HotelesViewController"] ) {
        
        HotelesViewController *temp=[appStoryboard instantiateViewControllerWithIdentifier:@"HotelesViewController"];
        [self.navigationController pushViewController:temp animated:NO];
        
    }else if ([item[@"controller"] isEqualToString:@"RecomendacionesViewController"] ) {
        
        RecomendacionesViewController *temp=[appStoryboard instantiateViewControllerWithIdentifier:@"RecomendacionesViewController"];
        [self.navigationController pushViewController:temp animated:NO];
        
    }else if ([item[@"controller"] isEqualToString:@"FotosVideosViewController"] ) {
        FotosVideosViewController *temp=[appStoryboard instantiateViewControllerWithIdentifier:@"FotosVideosViewController"];
        [self.navigationController pushViewController:temp animated:NO];
    }
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
