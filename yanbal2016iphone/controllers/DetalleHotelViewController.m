//
//  DetalleHotelViewController.m
//  yanbal2016iphone
//
//  Created by Jeisson González on 12/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import "DetalleHotelViewController.h"
@import Firebase;

@interface DetalleHotelViewController ()

@end

@implementation DetalleHotelViewController{
    NSMutableArray *recipies;
}

- (void)viewDidLoad {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.barTintColor = colorBlanco;
    self.navigationController.navigationBar.translucent = NO;
    
    UIImage *image=[UIImage imageNamed:_detalle[@"interna"]];
    
    //int width=((IS_IPHONE_6P)?960:((IS_IPHONE_6)?640:320));
    //int height=image.size.height*((IS_IPHONE_6P)?3:((IS_IPHONE_6)?2:1));
    
    CGRect frame = _hFlowView.frame;
    frame.size.height = [self getsize:177];
    frame.size.width = SCREEN_WIDTH;
    _hFlowView.frame = frame;
    
    
    frame = _hPageControl.frame;
    frame.origin.y=_hFlowView.frame.size.height-frame.size.height;
    _hPageControl.frame = frame;
    
    
    int height=(((IS_IPHONE_6P)?414:((IS_IPHONE_6)?375:320))*image.size.height)/320;
    
    
    CGRect myImageRect = CGRectMake(0.0f, _hFlowView.frame.size.height, ((IS_IPHONE_6P)?414:((IS_IPHONE_6)?375:320)),height);
    _imagen.frame=myImageRect;
    _imagen.image=image;
    
    height+=_hFlowView.frame.size.height+20;
    
    
    _btnHotel.frame=CGRectMake(_btnHotel.frame.origin.x,height,
                              _btnHotel.frame.size.width, _btnHotel.frame.size.height);
    
    height+=_btnHotel.frame.size.height+20;
    
    
    [_scrollView setContentSize:(CGSizeMake(_scrollView.frame.size.width, height))];
   
    
    if (!self.navigationItem.rightBarButtonItem) {
        UIImage* image = [UIImage imageNamed:@"icon_header_menu"];
        CGRect frameimg = CGRectMake(0, 0, image.size.width, image.size.height);
        UIButton *button = [[UIButton alloc] initWithFrame:frameimg];
        button.tintColor=[UIColor whiteColor];
        [button setBackgroundImage:image forState:UIControlStateNormal];
        [button addTarget:self action:@selector(showMenu) forControlEvents:UIControlEventTouchUpInside];
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    }
    
    
    [FIRAnalytics logEventWithName:accion_hotelesInterna parameters:@{ @"interna":  _detalle[@"nombre"]}];
    
    
    
    recipies=[[NSMutableArray alloc]initWithArray:_detalle[@"galeria"]];
    
    _hFlowView.delegate = self;
    _hFlowView.dataSource = self;
    _hFlowView.pageControl = _hPageControl;
    _hFlowView.minimumPageAlpha = 1;
    _hFlowView.minimumPageScale = 1;
    _hFlowView.orientation = PagedFlowViewOrientationHorizontal;
    [_hFlowView reloadData];
    [super viewDidLoad];
    //self.navigationController.navigationBar.barTintColor=[UIColor redColor];
    //NavBar tint color for elements:
    self.navigationController.navigationBar.tintColor=colorYanbal;
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"Icon-Back"]
                                                                   style:UIBarButtonItemStylePlain
                                                                  target:self.navigationController
                                                                  action:@selector(popViewControllerAnimated:)];
    self.navigationItem.leftBarButtonItem = backButton;
}



-(float)getsize:(int)value{
    
    float total=(value*SCREEN_WIDTH)/320;
    
    if(IS_IPHONE_6P || IS_IPHONE_6){
        return total;
    }else{
        return value;
    }
    
}

- (void)showMenu
{
    // Dismiss keyboard (optional)
    //
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    [self.frostedViewController presentMenuViewController];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark PagedFlowView Delegate
- (CGSize)sizeForPageInFlowView:(PagedFlowView *)flowView;{
    return CGSizeMake(_hFlowView.frame.size.width, _hFlowView.frame.size.height);
}

- (void)flowView:(PagedFlowView *)flowView didScrollToPageAtIndex:(NSInteger)index {
    NSLog(@"Scrolled to page # %ld", (long)index);
}

- (void)flowView:(PagedFlowView *)flowView didTapPageAtIndex:(NSInteger)index{
    NSLog(@"Scrolled to page # %ld", (long)index);
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark PagedFlowView Datasource
//返回显示View的个数
- (NSInteger)numberOfPagesInFlowView:(PagedFlowView *)flowView{
    return [recipies count];
}

//返回给某列使用的View
- (UIView *)flowView:(PagedFlowView *)flowView cellForPageAtIndex:(NSInteger)index{
    UIImageView *imageView = (UIImageView *)[flowView dequeueReusableCell];
    if (!imageView) {
        imageView = [[UIImageView alloc] init];
    }
    
    NSString *item=[recipies objectAtIndex:index];
    
    imageView.contentMode=UIViewContentModeScaleAspectFill;
    imageView.clipsToBounds = NO;
    imageView.backgroundColor=[UIColor whiteColor];
    [imageView setFrame:CGRectMake(0, 0, SCREEN_WIDTH, _hFlowView.frame.size.height)];
    imageView.image=[UIImage imageNamed:item];
    
    return imageView;
}


- (IBAction)pageControlValueDidChange:(id)sender {
    UIPageControl *pageControl = sender;
    [_hFlowView scrollToPage:pageControl.currentPage];
}
- (IBAction)abrirLink:(id)sender {
    
    [FIRAnalytics logEventWithName:accion_hotelesInterna parameters:@{ @"link":  _detalle[@"url"]}];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:_detalle[@"url"]]];
}

@end
