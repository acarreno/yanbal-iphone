//
//  FotosVideosViewController.h
//  yanbal2016iphone
//
//  Created by Jeisson González on 13/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"

@interface FotosVideosViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>


@property (strong, nonatomic) IBOutlet UITableView *table;

@property (strong, nonatomic) IBOutlet UIView *viewBienvenida;
@property (strong, nonatomic) IBOutlet UIView *viewDia1;
@property (strong, nonatomic) IBOutlet UIView *viewDia2;
@property (strong, nonatomic) IBOutlet UIView *viewDia3;
@property (strong, nonatomic) IBOutlet UIView *viewDia4;

@property (strong, nonatomic) IBOutlet UIImageView *imgNoResultados;
@end
