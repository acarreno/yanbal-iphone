//
//  ConcursoViewController.m
//  yanbal2016iphone
//
//  Created by Jeisson González on 19/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import "ConcursoViewController.h"
#import "VisorImagenesViewController.h"
#import "CellConcurso.h"

#import "UIImageView+WebCache.h"
#import "Services.h"
#import "JSNDataManager.h"

#import <QuartzCore/QuartzCore.h>
@import Firebase;

@interface ConcursoViewController ()

@end

@implementation ConcursoViewController{
    NSMutableArray *recipes;
    BOOL progress;
    NSString *imagenSeleccionada;
}

- (void)viewDidLoad {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.barTintColor = colorBlanco;
    self.navigationController.navigationBar.translucent = NO;
    
    [self initView];
    
    NSDictionary *verAyuda=[JSNDataManager getDictionary:fileAyuda];
    
    if (![verAyuda[@"estado"] isKindOfClass:[NSString class]]) {
        
        [self mostrarCoach];
    }else{
        [self.navigationController setNavigationBarHidden:NO animated:YES];
        [self cargarInformacion];
    }
    
    self.navigationItem.hidesBackButton = YES;
    [FIRAnalytics logEventWithName:accion_concursos parameters:nil];
}

-(void)cargarInformacion{
    
    progress=NO;
    recipes=[JSNDataManager getList:fileConcurso];
    if (recipes.count==0) {
        progress=YES;
    }
    
    [self serviceNoticias:^{
        [_table reloadData];
    }];
}

-(void)mostrarCoach{
    
    [self.navigationController setNavigationBarHidden:YES animated:YES];
    
    UIImageView *coachmak =[[UIImageView alloc] initWithFrame:CGRectMake(0,0,[UIScreen mainScreen].bounds.size.width,[UIScreen mainScreen].bounds.size.height)];
    
    if (IS_IPHONE_4_OR_LESS) {
        coachmak.image=[UIImage imageNamed:@"CoachMark_concurso_4S"];
    }else{
        coachmak.image=[UIImage imageNamed:@"CoachMark_concurso"];
    }
    
    coachmak.contentMode=UIViewContentModeRedraw;
    
    coachmak.layer.opacity=0;
    [self.view addSubview:coachmak];
    
    [UIView animateWithDuration:0.6
                     animations:^(void) {
                         coachmak.layer.opacity=1;
                     } completion:nil];
    
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cerrarCoahmak:)];
    singleTap.numberOfTapsRequired = 1;
    singleTap.numberOfTouchesRequired = 1;
    [coachmak addGestureRecognizer:singleTap];
    [coachmak setUserInteractionEnabled:YES];
    
    [JSNDataManager saveDictionary:@{
                                     @"estado":@"ok"
                                     } widthName:fileAyuda];
}

- (void)cerrarCoahmak:(UIGestureRecognizer *)gestureRecognizer {
    
    
    [self cargarInformacion];
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [UIView animateWithDuration:0.6
                     animations:^(void) {
                         gestureRecognizer.view.layer.opacity=0;
                     } completion:^(BOOL finished) {
                         [gestureRecognizer.view removeFromSuperview];
                     }];
    
}


-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // Navigation button was pressed. Do some stuff
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    [super viewWillDisappear:animated];
}

-(void)backToHome{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)showMenu
{
    // Dismiss keyboard (optional)
    //
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    [self.frostedViewController presentMenuViewController];
}

-(void)initView{
    
    
    
    _table.separatorColor = [UIColor clearColor];
    _table.backgroundColor=[UIColor clearColor];
    
}


-(void)serviceNoticias:(void (^)(void))completion{
    [Services getConcurso:serv_concurso delegate:self widthProgress:progress completion:^(BOOL finished, NSDictionary *res) {
        if (finished) {
            
            //NSLog(@"post: %@",res);
            
            
            NSMutableArray *tempList=[[NSMutableArray alloc]initWithArray:res[@"rows"]];
            
            recipes =[[NSMutableArray alloc]init];
            
            for (NSDictionary *item in tempList) {
                NSDictionary *nuevo=@{
                                      @"images":item[@"images"],
                                      @"from_full_name":item[@"from_full_name"],
                                      @"description":item[@"description"],
                                      @"source":item[@"source"],
                                      @"profile_url":item[@"profile_url"],
                                      @"target_url":((item[@"target_url"])?item[@"target_url"]:item[@"profile_url"])
                                      };
                [recipes addObject:nuevo];
            }
            
            [JSNDataManager saveList:recipes widthName:fileConcurso callback:^(NSString *fileName) {
                NSLog(@"Actualizado!!");
            }];
            completion();
        }else{
            NSLog(@"Error !!");
        }
    }];
    
}

#pragma mark -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [recipes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *item=[recipes objectAtIndex:indexPath.row];
    
    NSString *simpleTableIdentifier = @"CellTipoText";
    
    if (item[@"images"] && [item[@"images"] count]>0) {
        simpleTableIdentifier = @"CellTipoImagen";
    }
    
    
    CellConcurso *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[CellConcurso alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.nombre.text=item[@"from_full_name"];
    cell.contenido.text=item[@"description"];
    
    
    cell.redSocial.image=[UIImage imageNamed:@"concurso_item_ind_facebook"];
    if ([item[@"source"] isEqualToString:@"Twitter"]) {
        cell.redSocial.image=[UIImage imageNamed:@"concurso_item_ind_twitter"];
    }
    
    NSString *urlImagen=(([item[@"images"] count]>0)?[item[@"images"] objectAtIndex:0][@"url"]:@"");
    
    cell.imagen.image=nil;
    cell.imagen.backgroundColor=[UIColor grayColor];
    if (![urlImagen isEqualToString:@""]) {
        
        UIActivityIndicatorView *cargando = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        CGRect frame = cargando.frame;
        frame.origin.x = cell.imagen.frame.size.width / 2 - frame.size.width / 2;
        frame.origin.y = cell.imagen.frame.size.height / 2 - frame.size.height / 2;
        
        cargando.frame = frame;
        cargando.hidesWhenStopped = YES;
        [cell.imagen addSubview:cargando];
        [cargando startAnimating];
        
        NSLog(@"cargando: %@",urlImagen);
        
        [cell.imagen sd_setImageWithURL:[NSURL URLWithString:urlImagen] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            for (UIView *sub in cell.imagen.subviews) {
                if ([sub isKindOfClass:[UIActivityIndicatorView class]]) {
                    [sub removeFromSuperview];
                }
            }
            cell.imagen.image=image;
            
            cell.imagen.tag=indexPath.row;
            
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(verImagen:)];
            singleTap.numberOfTapsRequired = 1;
            singleTap.numberOfTouchesRequired = 1;
            [cell.imagen addGestureRecognizer:singleTap];
            [cell.imagen setUserInteractionEnabled:YES];
            
        }];
    }
    
    
    NSString *urlPerfil=item[@"profile_url"];
    cell.perfil.image=nil;
    cell.perfil.backgroundColor=[UIColor grayColor];
    if (![urlPerfil isEqualToString:@""]) {
        UIActivityIndicatorView *cargando = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        CGRect frame = cargando.frame;
        frame.origin.x = cell.perfil.frame.size.width / 2 - frame.size.width / 2;
        frame.origin.y = cell.perfil.frame.size.height / 2 - frame.size.height / 2;
        
        cargando.frame = frame;
        cargando.hidesWhenStopped = YES;
        [cell.perfil addSubview:cargando];
        [cargando startAnimating];
        
        [cell.perfil sd_setImageWithURL:[NSURL URLWithString:urlPerfil] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            for (UIView *sub in cell.perfil.subviews) {
                if ([sub isKindOfClass:[UIActivityIndicatorView class]]) {
                    [sub removeFromSuperview];
                }
            }
            cell.perfil.image=image;
        }];
        
    }
    
    cell.btnCompartir.tag=indexPath.row;
    [cell.btnCompartir addTarget:self action:@selector(clickCompartir:) forControlEvents:UIControlEventTouchUpInside];
    
    
    cell.bgContent.layer.cornerRadius=2;
    cell.bgContent.layer.masksToBounds = NO;
    cell.bgContent.layer.shadowOffset = CGSizeMake(1, 0);
    cell.bgContent.layer.shadowRadius = 2;
    cell.bgContent.layer.shadowOpacity = 0.5;
    
    
    return cell;
}

-(void)clickCompartir:(UIButton *)sender
{
    
    NSDictionary *item=[recipes objectAtIndex:(int)sender.tag];
    
    NSLog(@"url: %@",item[@"target_url"]);
    
    NSString *urlString=@"";
    
    if (item[@"target_url"]) {
        urlString=item[@"target_url"];
    }else{
        urlString=item[@"images"][0][@"url"];
    }
    
    
    NSString *descripcion=[NSString stringWithFormat:@"#SoySmartSoyYanbal @eventosyanbal "];
    NSURL *url=[NSURL URLWithString:urlString];
    NSMutableArray *itemsToShare=[[NSMutableArray alloc]initWithObjects:descripcion,url, nil];
    [self compartirEnRedes:itemsToShare];
    
}

- (IBAction)publicar:(id)sender {
    
    
    [FIRAnalytics logEventWithName:accion_compartirConcuros parameters:nil];
    
    NSString *descripcion=[NSString stringWithFormat:@"#SoySmartSoyYanbal @eventosyanbal "];
    NSURL *url=[NSURL URLWithString:@"http://eventosyanbal.com/"];
    NSMutableArray *itemsToShare=[[NSMutableArray alloc]initWithObjects:descripcion,url, nil];
    [self compartirEnRedes:itemsToShare];
}

-(void)compartirEnRedes:(NSArray *)itemsToShare{
    
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:[[NSArray alloc]initWithArray:itemsToShare]
                                                                             applicationActivities:nil];
    
    activityVC.excludedActivityTypes = @[UIActivityTypePostToWeibo,
                                         UIActivityTypeMessage,
                                         UIActivityTypePrint,
                                         UIActivityTypeCopyToPasteboard,
                                         UIActivityTypeAssignToContact,
                                         UIActivityTypeSaveToCameraRoll,
                                         UIActivityTypeAddToReadingList,
                                         UIActivityTypePostToFlickr,
                                         UIActivityTypePostToVimeo,
                                         UIActivityTypePostToTencentWeibo,
                                         UIActivityTypeAirDrop];
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (void)verImagen:(UITapGestureRecognizer *)recognizer {
    
    NSDictionary *item=[recipes objectAtIndex:(int)recognizer.view.tag];
    imagenSeleccionada = [item[@"images"] objectAtIndex:0][@"url"];
    [self performSegueWithIdentifier:@"segMostrarGaleria" sender:self];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    NSDictionary *item=[recipes objectAtIndex:indexPath.row];
    
    if (item[@"images"] && [item[@"images"] count]>0){//con imagen
        if (IS_IPHONE_5) {
            return 250;
        }else if(IS_IPHONE_6){
            return 293;
        }else if(IS_IPHONE_6P){
            return 323;
        }else{
            return 250;
        }
    }else{//solo texto
        return 145;
    }
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    VisorImagenesViewController *temp= segue.destinationViewController;
    [temp setRecipies:[[NSMutableArray alloc]initWithObjects:imagenSeleccionada, nil]];
    [temp setIndexImage:[NSString stringWithFormat:@"%i",0]];
}


@end
