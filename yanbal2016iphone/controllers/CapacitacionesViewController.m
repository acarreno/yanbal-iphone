//
//  CapacitacionesViewController.m
//  yanbal2016iphone
//
//  Created by Jeisson González on 18/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import "CapacitacionesViewController.h"
#import "DetalleFotosVideosViewController.h"
#import "CellFotosVideos.h"

#import "UIImageView+WebCache.h"
#import "Services.h"
#import "JSNDataManager.h"

@import Firebase;


@interface CapacitacionesViewController ()

@end

@implementation CapacitacionesViewController{
    NSMutableArray *recipes;
    NSDictionary *seleccionado;
    BOOL progress;
}

- (void)viewDidLoad {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationController.navigationBar.barTintColor = colorBlanco;
    self.navigationController.navigationBar.translucent = NO;
    
    
    [self.navigationController setNavigationBarHidden:NO animated:YES];
    [self initView];
    
    _table.separatorColor = [UIColor clearColor];
    _table.backgroundColor=[UIColor clearColor];
    
    progress=NO;
    recipes=[JSNDataManager getList:fileCapacitaciones];
    if (recipes.count==0) {
        progress=YES;
    }
    
    [self serviceNoticias:^{
        [_table reloadData];
    }];
    
    self.navigationItem.hidesBackButton = YES;
    [FIRAnalytics logEventWithName:accion_entrenamiento parameters:nil];
}

-(void) viewWillDisappear:(BOOL)animated {
    if ([self.navigationController.viewControllers indexOfObject:self]==NSNotFound) {
        // Navigation button was pressed. Do some stuff
        [self.navigationController setNavigationBarHidden:YES animated:YES];
    }
    [super viewWillDisappear:animated];
}

-(void)backToHome{
    [self.navigationController popViewControllerAnimated:YES];
}


- (void)showMenu
{
    // Dismiss keyboard (optional)
    //
    [self.view endEditing:YES];
    [self.frostedViewController.view endEditing:YES];
    
    [self.frostedViewController presentMenuViewController];
}

-(void)initView{
    
    
}


-(void)serviceNoticias:(void (^)(void))completion{
    
    NSString *urlCategorias=serv_categorias;
    
    urlCategorias=[urlCategorias stringByReplacingOccurrencesOfString:@"{{idCategoria}}" withString:@"6"];
    
    NSLog(@"consultando: %@",urlCategorias);
    
    [Services get:urlCategorias delegate:self widthProgress:progress completion:^(BOOL finished, NSDictionary *res) {
        if (finished && [res[@"posts"] count]>0) {
            
            NSMutableArray *tempList=[[NSMutableArray alloc]initWithArray:res[@"posts"]];
            
            recipes =[[NSMutableArray alloc]init];
            
            for (NSDictionary *item in tempList) {
                NSDictionary *nuevo=@{
                                      @"url":item[@"url"],
                                      @"titulo":item[@"title"],
                                      @"fecha":((item[@"date"])?item[@"date"]:@""),
                                      @"texto":item[@"content"],
                                      @"descripcion":((item[@"custom_fields"][@"descripcion"])?item[@"custom_fields"][@"descripcion"]:@""),
                                      @"imagen":((item[@"thumbnail_images"][@"full"][@"url"])?item[@"thumbnail_images"][@"full"][@"url"]:@""),
                                      @"galeria_imagenes":((item[@"custom_fields"][@"galeria_de_imagenes"])?item[@"custom_fields"][@"galeria_de_imagenes"]:[[NSArray alloc]init]),
                                      @"galeria_videos":((item[@"custom_fields"][@"galeria_de_videos"])?item[@"custom_fields"][@"galeria_de_videos"]:[[NSArray alloc]init]),
                                      };
                [recipes addObject:nuevo];
            }
            
            [JSNDataManager saveList:recipes widthName:fileCapacitaciones callback:^(NSString *fileName) {
                NSLog(@"Actualizado!!");
            }];
            completion();
        }else{
            NSLog(@"Error !!");
        }
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [recipes count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *item=[recipes objectAtIndex:indexPath.row];
    
    static NSString *simpleTableIdentifier = @"CellFotosVideos";
    
    CellFotosVideos *cell = [tableView dequeueReusableCellWithIdentifier:simpleTableIdentifier];
    
    if (cell == nil) {
        cell = [[CellFotosVideos alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:simpleTableIdentifier];
    }
    
    cell.titulo.text=item[@"titulo"];
    cell.descripcion.text=item[@"descripcion"];
    
    NSString *urlImagen=item[@"imagen"];
    //NSLog(@"Cargando: %@",urlImagen);
    
    cell.imagen.image=nil;
    if (![urlImagen isEqualToString:@""]) {
        UIActivityIndicatorView *cargando = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        
        
        CGRect frame = cargando.frame;
        frame.origin.x = cell.imagen.frame.size.width / 2 - frame.size.width / 2;
        frame.origin.y = cell.imagen.frame.size.height / 2 - frame.size.height / 2;
        
        cargando.frame = frame;
        cargando.hidesWhenStopped = YES;
        [cell.imagen addSubview:cargando];
        [cargando startAnimating];
        
        [cell.imagen sd_setImageWithURL:[NSURL URLWithString:urlImagen] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
            
            for (UIView *sub in cell.imagen.subviews) {
                if ([sub isKindOfClass:[UIActivityIndicatorView class]]) {
                    [sub removeFromSuperview];
                }
            }
            cell.imagen.image=image;
        }];
        
    }
    
    
    cell.backgroundColor=[UIColor clearColor];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (IS_IPHONE_5) {
        return 195;
    }else if(IS_IPHONE_6){
        return 235;
    }else if(IS_IPHONE_6P){
        return 252;
    }else{
        return 195;
    }
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    seleccionado=[recipes objectAtIndex:indexPath.row];
    NSLog(@"element: %@",seleccionado);
    [self performSegueWithIdentifier:@"segDetalleFotosVideos" sender:self];
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    DetalleFotosVideosViewController *temp=segue.destinationViewController;
    [temp setDetalle:seleccionado];
}

@end
