//
//  CapacitacionesViewController.h
//  yanbal2016iphone
//
//  Created by Jeisson González on 18/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"

@interface CapacitacionesViewController : UIViewController<UITableViewDelegate, UITableViewDataSource>


@property (strong, nonatomic) IBOutlet UITableView *table;

@end
