//
//  MenuViewController.h
//  yanbal2016iphone
//
//  Created by Jeisson González on 11/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"

@interface MenuViewController : UIViewController


@property (strong, nonatomic) IBOutlet UITableView *table;
@property (strong, nonatomic) IBOutlet NSMutableArray *recipes;

@end
