//
//  InicioViewController.m
//  yanbal2016iphone
//
//  Created by IMAC on 28/02/18.
//  Copyright © 2018 wigilabs. All rights reserved.
//

#import "InicioViewController.h"
@import Firebase;
@interface InicioViewController ()

@end

@implementation InicioViewController

- (void)viewDidLoad {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self initView];
    
}

-(void)initView{
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    if (IS_IPHONE_X){
        CGRect frame=_topView.frame;
        frame.size.height=_topView.frame.size.height+20;
        _topView.frame=frame;
    }
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated
{
    [FIRAnalytics logEventWithName:accion_inicio parameters:nil];
    [super viewWillAppear:animated];
    NSLog(@"view will Appear");
    [UIView animateWithDuration:20
                     animations:^{
                         _bg.transform=CGAffineTransformMakeScale(1.5f, 1.5f);
                         
                         
                     }
                     completion:^(BOOL finished){ [UIView animateWithDuration:60
                                                                   animations:^{
                                                                       _bg.transform=CGAffineTransformMakeScale(1.0f, 1.0f);
                                                                       
                                                                       
                                                                   }
                                                                   completion:^(BOOL finished){ }]; }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
