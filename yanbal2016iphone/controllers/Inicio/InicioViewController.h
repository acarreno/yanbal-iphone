//
//  InicioViewController.h
//  yanbal2016iphone
//
//  Created by IMAC on 28/02/18.
//  Copyright © 2018 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"


@interface InicioViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (strong, nonatomic) IBOutlet UIImageView *bg;
@end
