//
//  LoginViewController.h
//  yanbal2016iphone
//
//  Created by Jeisson González on 12/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITextField *txtCod;
@property (strong, nonatomic) IBOutlet UILabel *lblTitulo;


@end
