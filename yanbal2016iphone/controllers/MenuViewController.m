//
//  MenuViewController.m
//  yanbal2016iphone
//
//  Created by Jeisson González on 11/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import "MenuViewController.h"

#import "HomeViewController.h"
#import "HorariosViewController.h"
#import "GuiaViewController.h"
#import "HotelesViewController.h"
#import "FotosVideosViewController.h"
#import "RecomendacionesViewController.h"
#import "ConcursoViewController.h"
#import "CapacitacionesViewController.h"
#import "ReporductorViewController.h"

#import "CellMenu.h"

#import "Services.h"

@import Firebase;
@interface MenuViewController ()

@end

@implementation MenuViewController{
    NSDictionary *usuario;
    int estado;
    NSDictionary *dataStreaming;
}

- (void)viewDidLoad {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    
    _recipes=[[NSMutableArray alloc]initWithObjects:
              @{
                @"id":@0,
                @"nombre":@"INICIO"
                },
              @{
                @"id":@1,
                @"nombre":@"HORARIOS"
                },
              @{
                @"id":@2,
                @"nombre":@"GUÍA DE LOOKS"
                },
              @{
                @"id":@3,
                @"nombre":@"HOTELES"
                },
              @{
                @"id":@4,
                @"nombre":@"FOTOS Y VIDEOS"
                },
              @{
                @"id":@5,
                @"nombre":@"RECOMENDACIONES"
                },
              @{
                @"id":@8,
                @"nombre":@"TRANSMISIÓN EN VIVO"
                },
              nil];
    
    
    _table.scrollEnabled = NO;
    if (IS_IPHONE_4_OR_LESS || IS_IPHONE_5) {
        _table.scrollEnabled = YES;
    }
    
    
    _table.separatorColor = [UIColor clearColor];
    _table.backgroundColor=[UIColor clearColor];
    [_table reloadData];
    
    
    [FIRAnalytics logEventWithName:accion_menuLateral parameters:nil];
}

-(void)viewDidAppear:(BOOL)animated{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    usuario= [defaults objectForKey:@"usuario"];
    
}



-(void)showLogin{
    
    dispatch_queue_t myq = dispatch_queue_create("myqueue", NULL);
    dispatch_async(myq, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UINavigationController *temp=[self.storyboard instantiateViewControllerWithIdentifier:@"navLoginViewController"];
            [self presentViewController:temp animated:YES completion:nil];
            
        });
    });
}

-(void)verStreaming{
    
    [Services get:serv_streaming delegate:self widthProgress:YES completion:^(BOOL finished, NSDictionary *res) {
        if (finished) {
            
            NSLog(@"res: %@",res);
            if (res[@"post"][@"custom_fields"] && res[@"post"][@"custom_fields"][@"descripcion"]) {
                if (![res[@"post"][@"custom_fields"][@"descripcion"] isEqualToString:@"none"]) {
                    
                    dispatch_queue_t myq = dispatch_queue_create("myqueue", NULL);
                    dispatch_async(myq, ^{
                        dispatch_async(dispatch_get_main_queue(), ^{
                            
                            NSLog(@"mostrar streaming");
                            dataStreaming=res[@"post"];
                            ReporductorViewController *temp=[self.storyboard instantiateViewControllerWithIdentifier:@"ReporductorViewController"];
                            [temp setData:dataStreaming];
                            [self presentViewController:temp animated:YES completion:nil];
                            
                        });
                    });
                    
                }else{
                    [self showEnEspera];
                }
            }else{
                [self showEnEspera];
            }
        }else{
            [self showEnEspera];
        }
    }];
}


-(void)showEnEspera{
    dispatch_queue_t myq = dispatch_queue_create("myqueue", NULL);
    dispatch_async(myq, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UINavigationController *temp=[self.storyboard instantiateViewControllerWithIdentifier:@"navEsperaViewController"];
            [self presentViewController:temp animated:YES completion:nil];
            
        });
    });
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)cerrar:(id)sender {
    [self.frostedViewController hideMenuViewController];
}




@end
