//
//  DetalleFotosVideosViewController.h
//  yanbal2016iphone
//
//  Created by Jeisson González on 14/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "REFrostedViewController.h"

@interface DetalleFotosVideosViewController : UIViewController

@property (strong, nonatomic) IBOutlet NSDictionary *detalle;
@property (strong, nonatomic) IBOutlet UIButton *btnCompartir;
@property (strong, nonatomic) IBOutlet UITextView *titulo;
@property (strong, nonatomic) IBOutlet UILabel *fecha;
@property (strong, nonatomic) IBOutlet UITextView *contenido;
@property (strong, nonatomic) IBOutlet UIScrollView *scrollView;
@end
