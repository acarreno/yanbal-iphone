//
//  LoginViewController.m
//  yanbal2016iphone
//
//  Created by Jeisson González on 12/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import "LoginViewController.h"
#import "Services.h"
#import "SVProgressHUD.h"
#import "ReporductorViewController.h"

@import Firebase;


@interface LoginViewController ()

@end

@implementation LoginViewController{
    NSDictionary *dataStreaming;
}

- (void)viewDidLoad {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.barTintColor = colorBlanco;
    self.navigationController.navigationBar.translucent = NO;
    
    
    UIButton *btnVolver =  [UIButton buttonWithType:UIButtonTypeCustom];
    [btnVolver setImage:[UIImage imageNamed:@"icon_atras"] forState:UIControlStateNormal];
    [btnVolver addTarget:self action:@selector(backToHome) forControlEvents:UIControlEventTouchUpInside];
    [btnVolver setFrame:CGRectMake(0, 0, 31, 31)];
    
    UIView *leftBarButtonItems = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 31, 31)];
    [leftBarButtonItems addSubview:btnVolver];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftBarButtonItems];
    
    [self.navigationController.navigationBar setBarTintColor:colorYanbal];
    
    [FIRAnalytics logEventWithName:accion_login parameters:nil];
    
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *usuario= [defaults objectForKey:@"usuario"];
    if (usuario[@"username"]) {
        [self verStreaming];
    }
    
}


-(void)backToHome{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)login:(id)sender {
    [self.view endEditing:YES];
    
    if (![_txtCod.text isEqualToString:@""]) {
        
        NSString *urlLogin=serv_login;
        urlLogin=[urlLogin stringByReplacingOccurrencesOfString:@"{{user}}" withString:_txtCod.text];
        
        
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
        [SVProgressHUD showWithStatus:@"Cargando..."];
        
        [Services get:urlLogin delegate:self widthProgress:YES completion:^(BOOL finished, NSDictionary *res) {
            [SVProgressHUD dismiss];
            if (finished) {
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                
                NSDictionary *temp=@{
                                     @"status":res[@"status"],
                                     @"username":res[@"user"][@"username"],
                                     @"codigo":res[@"user"][@"displayname"],
                                     @"cookie":res[@"cookie"]
                                     };
                
                [defaults setObject:temp forKey:@"usuario"];
                [defaults synchronize];
                
                
                [FIRAnalytics logEventWithName:accion_loginOK parameters:@{ @"codigo": _txtCod.text }];
                
                [self verStreaming];
                
            }
        }];
    }else{
        UIAlertController *alert=[UIAlertController
                                  alertControllerWithTitle:@""
                                  message:@"Debes ingresar tu código de directora para acceder a este contenido."
                                  preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction* yesButton = [UIAlertAction
                                    actionWithTitle:@"OK"
                                    style:UIAlertActionStyleDefault
                                    handler:^(UIAlertAction * action)
                                    {
                                        //Handel your yes please button action here
                                        [alert dismissViewControllerAnimated:YES completion:nil];
                                        [_txtCod becomeFirstResponder];
                                        
                                    }];
        
        [alert addAction:yesButton];
        [self presentViewController:alert animated:YES completion:nil];
    }
    
}


-(void)verStreaming{
    
    
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD showWithStatus:@"Cargando..."];
    
    [Services get:serv_streaming delegate:self widthProgress:YES completion:^(BOOL finished, NSDictionary *res) {
        [SVProgressHUD dismiss];
        
        if (finished) {
            
            NSLog(@"res: %@",res);
            if (res[@"post"][@"custom_fields"] && res[@"post"][@"custom_fields"][@"descripcion"]) {
                if (![res[@"post"][@"custom_fields"][@"descripcion"] isEqualToString:@"none"]) {
                    
                    NSLog(@"mostrar streaming");
                    dataStreaming=res[@"post"];
                    [self performSegueWithIdentifier:@"segStreaming" sender:self];
                    
                }else{
                    [self performSegueWithIdentifier:@"segNoTrasmision" sender:self];
                }
            }else{
                [self performSegueWithIdentifier:@"segNoTrasmision" sender:self];
            }
        }else{
            [self performSegueWithIdentifier:@"segNoTrasmision" sender:self];
        }
    }];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    
    if ([segue.identifier isEqualToString:@"segStreaming"]) {
        
        ReporductorViewController *temp= segue.destinationViewController;
        [temp setData:dataStreaming];
    }
    
    
}

@end
