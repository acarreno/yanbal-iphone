//
//  VisorImagenesViewController.m
//  yanbal2016iphone
//
//  Created by Jeisson González on 18/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import "VisorImagenesViewController.h"
#import "UIImageView+WebCache.h"

@interface VisorImagenesViewController ()

@end

@implementation VisorImagenesViewController

- (void)viewDidLoad {
    self.navigationItem.backBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"" style:UIBarButtonItemStylePlain target:nil action:nil];
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    _hFlowView.delegate = self;
    _hFlowView.dataSource = self;
    _hFlowView.pageControl = _hPageControl;
    _hFlowView.minimumPageAlpha = 1;
    _hFlowView.minimumPageScale = 1;
    _hFlowView.orientation = PagedFlowViewOrientationHorizontal;
    [_hFlowView reloadData];
    
    
}

-(void)viewDidAppear:(BOOL)animated{
    
    _hPageControl.currentPage=[_indexImage intValue];
    [_hFlowView scrollToPage:[_indexImage intValue]];
    
    [super viewDidAppear:animated];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark PagedFlowView Delegate
- (CGSize)sizeForPageInFlowView:(PagedFlowView *)flowView;{
    return CGSizeMake(_hFlowView.frame.size.width, _hFlowView.frame.size.height);
}

- (void)flowView:(PagedFlowView *)flowView didScrollToPageAtIndex:(NSInteger)index {
    NSLog(@"Scrolled to page # %ld", (long)index);
}

- (void)flowView:(PagedFlowView *)flowView didTapPageAtIndex:(NSInteger)index{
    NSLog(@"Scrolled to page # %ld", (long)index);
    
}

////////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark -
#pragma mark PagedFlowView Datasource
//返回显示View的个数
- (NSInteger)numberOfPagesInFlowView:(PagedFlowView *)flowView{
    return [_recipies count];
}

//返回给某列使用的View
- (UIView *)flowView:(PagedFlowView *)flowView cellForPageAtIndex:(NSInteger)index{
    UIImageView *imageView = (UIImageView *)[flowView dequeueReusableCell];
    if (!imageView) {
        imageView = [[UIImageView alloc] init];
    }
    
    NSString *item=[_recipies objectAtIndex:index];
    
    imageView.contentMode=UIViewContentModeScaleAspectFit;
    imageView.clipsToBounds = NO;
    imageView.backgroundColor=[UIColor blackColor];
    [imageView setFrame:CGRectMake(0, 0, _hFlowView.frame.size.width, _hFlowView.frame.size.height)];
    
    UIActivityIndicatorView *cargando = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    CGRect frame = cargando.frame;
    frame.origin.x = imageView.frame.size.width / 2 - frame.size.width / 2;
    frame.origin.y = imageView.frame.size.height / 2 - frame.size.height / 2;
    cargando.frame = frame;
    cargando.hidesWhenStopped = YES;
    [imageView addSubview:cargando];
    [cargando startAnimating];
    
    
    [imageView  sd_setImageWithURL:[NSURL URLWithString:item] completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        
        for (UIView *sub in imageView.subviews) {
            if ([sub isKindOfClass:[UIActivityIndicatorView class]]) {
                [sub removeFromSuperview];
            }
        }
        
        imageView.image=image;
    }];
    
    return imageView;
}

- (IBAction)pageControlValueDidChange:(id)sender {
    UIPageControl *pageControl = sender;
    [_hFlowView scrollToPage:pageControl.currentPage];
}

- (IBAction)salir:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
