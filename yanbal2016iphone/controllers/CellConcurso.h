//
//  CellConcurso.h
//  yanbal2016iphone
//
//  Created by Jeisson González on 19/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CellConcurso : UITableViewCell
@property (strong, nonatomic) IBOutlet UIView *bgContent;
@property (strong, nonatomic) IBOutlet UIImageView *perfil;
@property (strong, nonatomic) IBOutlet UILabel *nombre;
@property (strong, nonatomic) IBOutlet UITextView *contenido;
@property (strong, nonatomic) IBOutlet UIImageView *imagen;
@property (strong, nonatomic) IBOutlet UIButton *btnCompartir;
@property (strong, nonatomic) IBOutlet UIImageView *redSocial;

@end
