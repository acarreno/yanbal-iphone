//
//  AppDelegate.m
//  yanbal2016iphone
//
//  Created by Jeisson González on 11/04/16.
//  Copyright © 2016 wigilabs. All rights reserved.
//

#import "AppDelegate.h"
@import Firebase;
@import OneSignal;

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    //
        for (NSString* family in [UIFont familyNames])
        {
            NSLog(@"%@", family);
    
            for (NSString* name in [UIFont fontNamesForFamilyName: family])
            {
                NSLog(@"  %@", name);
            }
        }
    
    // Initialize Firebase.
    [FIRApp configure];
    
    
    //PUSH NOTIFICATIONS
    
    
    [OneSignal initWithLaunchOptions:launchOptions appId:@"6ac4a9d0-8e2d-4fdf-aeff-883340d9205f" handleNotificationReceived:^(OSNotification *notification) {
        NSLog(@"Received Notification - %@", notification.payload.notificationID);
    } handleNotificationAction:^(OSNotificationOpenedResult *result) {
        
        // This block gets called when the user reacts to a notification received
        OSNotificationPayload* payload = result.notification.payload;
        
        NSString* messageTitle = @"OneSignal Example";
        NSString* fullMessage = [payload.body copy];
        
        NSLog(@"messageTitle: %@",messageTitle);
        NSLog(@"fullMessage: %@",fullMessage);
        NSLog(@"aditional data: %@",payload.additionalData);
        
        if (payload.additionalData) {
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:payload.additionalData forKey:userNotification];
            [defaults synchronize];
        }
        
    } settings:@{kOSSettingsKeyInFocusDisplayOption : @(OSNotificationDisplayTypeNotification), kOSSettingsKeyAutoPrompt : @YES}];
    
    [OneSignal IdsAvailable:^(NSString *userId, NSString *pushToken) {
        if(pushToken) {
            NSLog(@"Received push token - %@", pushToken);
            NSLog(@"User ID - %@", userId);
        }
    }];
    
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    
    int margen= -24;
    
    if (IS_IPHONE_X) {
        margen = -45;
    }
    
    
    UIView *bgTop= [[UIView alloc]initWithFrame:CGRectMake(0, margen, SCREEN_WIDTH, (margen * -1) )];
    bgTop.backgroundColor=colorNegro;
    
    UIView *viewLinea= [[UIView alloc]initWithFrame:CGRectMake(0, bgTop.frame.size.height-3, SCREEN_WIDTH, 3)];
    viewLinea.backgroundColor=colorYanbal;
    [bgTop addSubview:viewLinea];
    
    
    [[UINavigationBar appearance] insertSubview:bgTop atIndex:10];
    
    
    [[UINavigationBar appearance] setTintColor:[UIColor whiteColor]];
    [[UINavigationBar appearance] setBarTintColor: colorBlanco];
    
    
    [[UINavigationBar appearance] setTitleTextAttributes:@{
                                                          NSForegroundColorAttributeName: colorYanbal,
                                                          NSFontAttributeName: [UIFont fontWithName:@"Futura-CondensedMedium" size:25.0f]
                                                          }];
    
    

    
    return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    
    
}

-(void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler{
    
    
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
