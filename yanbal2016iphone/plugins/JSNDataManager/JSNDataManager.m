//
//  JSNDataManager.m
//  PONDS
//
//  Created by Jeisson González on 23/05/14.
//
//

#import "JSNDataManager.h"

@implementation JSNDataManager


//inicio:listas
+ (void)saveList:(NSMutableArray*)NSArray widthName:(NSString*)fileName callback:(void (^)(NSString *fileName))callback
{
    
//    dispatch_queue_t myq = dispatch_queue_create("myqueue", NULL);
//    dispatch_async(myq, ^{
//        dispatch_async(dispatch_get_main_queue(), ^{
//            
//        });
//    });
    
    NSString *error;
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *plistPath = [rootPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist",fileName]];
    NSData *plistData = [NSPropertyListSerialization dataFromPropertyList:NSArray
                                                                   format:NSPropertyListXMLFormat_v1_0
                                                         errorDescription:&error];
    if(plistData) {
        [plistData writeToFile:plistPath atomically:YES];
        
        if(callback)
            callback(fileName);
    }
}

+ (NSMutableArray *)getList:(NSString*)fileName{
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *path = [rootPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist",fileName]];
    //NSLog(@"Buscando archivo: %@",path);
    NSMutableArray *lista= [[NSMutableArray alloc] initWithContentsOfFile:path];
    if (nil==lista) {
        return [[NSMutableArray alloc]init];
    }else{
        return lista;
    }
}

+ (NSMutableArray *)getList:(NSString*)fileName byCountry:(NSString *)country{
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *path = [rootPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist",fileName]];
    //NSLog(@"Buscando archivo: %@",path);
    NSMutableArray *lista= [[NSMutableArray alloc] initWithContentsOfFile:path];
    if (nil==lista) {
        return [[NSMutableArray alloc]init];
    }else{
        
        NSMutableArray *listPais= [[NSMutableArray alloc] init];
        
        for (NSDictionary *item in lista) {
            if ([item[country] intValue]==1) {
                [listPais addObject:item];
            }
        }
        
        return listPais;
    }
}


+ (BOOL)findElement:(NSDictionary*)element inList:(NSMutableArray *)lista{
    for (NSDictionary *item in lista) {
        if ([item isEqualToDictionary:element]) {
            return YES;
        }
    }
    return NO;
}

+ (void)deleteFile:(NSString*)fileName{
    NSError *error;
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *path = [rootPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist",fileName]];
    [[NSFileManager defaultManager] removeItemAtPath: path error: &error];
}
//fin:listas

//inicio:diccionario
+ (void)saveDictionary:(NSDictionary*)NSDictionary widthName:(NSString*)fileName
{
    
    dispatch_queue_t myq = dispatch_queue_create("myqueue", NULL);
    dispatch_async(myq, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            NSString *error;
            NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
            NSString *plistPath = [rootPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist",fileName]];
            NSData *plistData = [NSPropertyListSerialization dataFromPropertyList:NSDictionary
                                                                           format:NSPropertyListXMLFormat_v1_0
                                                                 errorDescription:&error];
            if(plistData) {
                [plistData writeToFile:plistPath atomically:YES];
            }
            NSLog(@"filePath sucess: %@",plistPath);
        });
    });
}

+ (NSDictionary*)getDictionary:(NSString*)fileName{
    NSString *rootPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *path = [rootPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.plist",fileName]];
    NSDictionary *lista= [[NSDictionary alloc] initWithContentsOfFile:path];
    if (nil==lista) {
        return [[NSDictionary alloc]init];
    }else{
        return lista;
    }
}
//fin:diccionario

//inicio:imagenes
+ (void)saveImage:(UIImage*)image widthName:(NSString*)fileName{
    
    dispatch_queue_t myq = dispatch_queue_create("myqueue", NULL);
    dispatch_async(myq, ^{
        dispatch_async(dispatch_get_main_queue(), ^{
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
            NSString *documentsDirectory = [paths objectAtIndex:0];
            NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",fileName]];
            NSData* data = UIImagePNGRepresentation(image);
            [data writeToFile:path atomically:YES];
        });
    });
}

+ (UIImage*)getImage:(NSString*)fileName{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",fileName]];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    return image;
}

+ (UIImage*)getImageFullName:(NSString*)fileName{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory,NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString* path = [documentsDirectory stringByAppendingPathComponent:fileName];
    UIImage* image = [UIImage imageWithContentsOfFile:path];
    return image;
}
//fin:imagenes





@end
