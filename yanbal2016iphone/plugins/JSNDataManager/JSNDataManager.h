//
//  JSNDataManager.h
//  PONDS
//
//  Created by Jeisson González on 23/05/14.
//
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface JSNDataManager : NSObject



//inicio:listas
+ (void)saveList:(NSMutableArray*)NSArray widthName:(NSString*)fileName callback:(void (^)(NSString *fileName))callback;
+ (NSMutableArray *)getList:(NSString*)fileName;
+ (NSMutableArray *)getList:(NSString*)fileName byCountry:(NSString *)country;
+ (BOOL)findElement:(NSDictionary*)element inList:(NSMutableArray *)lista;
+ (void)deleteFile:(NSString*)fileName;
//fin:listas

//inicio:diccionario
+ (void)saveDictionary:(NSDictionary*)NSDictionary widthName:(NSString*)fileName;
+ (NSDictionary*)getDictionary:(NSString*)fileName;
//fin:diccionario

//inicio:imagenes
+ (void)saveImage:(UIImage*)image widthName:(NSString*)fileName;
+ (UIImage*)getImage:(NSString*)fileName;
+ (UIImage*)getImageFullName:(NSString*)filelName;
//fin:imagenes

//inicio:zip
+ (NSString *)unzipping:(NSString *)fileName;
//fin:zip


@end
