//
//  CameraViewController.swift
//  yanbal2016ipad
//
//  Created by Jeisson González on 27/02/18.
//  Copyright © 2018 wigilabs. All rights reserved.
//

import UIKit
import AVFoundation
import Firebase

@available(iOS 8.0, *)
class BackCameraController: UIViewController,  UICollectionViewDataSource, UICollectionViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    @IBOutlet var collection: UICollectionView!
    @IBOutlet var previewView: UIView!
    @IBOutlet var captureImageView: UIImageView!
    @IBOutlet var marco: UIImageView!
    var selectedMarco:String = ""
    var cambioDecamara = false
    var cameraSettings=CameraSettings()
    var imagePicker = UIImagePickerController()
    @IBOutlet var screenShotArea: UIView!
    var captureSession: AVCaptureSession?
    var stillImageOutput: AVCaptureStillImageOutput?
    var previewLayer: AVCaptureVideoPreviewLayer?
   var image=UIImage()
    let structura = Marcos()
    
    var filtros:[String]=[]
    @IBOutlet var topBar: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        //previewLayer?.frame = CGRect(x: 0, y:0 , width: screenWidth, height:screenHeight)
        //previewView.frame = CGRect(x: 0, y:-60 , width: screenWidth, height:screenHeight-60)
        //previewView.frame.height = screenHeight
    }
    func initView(){
        filtros=structura.marcos
        
        let layout = collection.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: 40, height: 50)
        if(selectedMarco==""){
            selectedMarco=filtros[0]
        }
        marco.image=UIImage(named:selectedMarco)
        previewLayer=cameraSettings.startCamera(isFront: false)
        
        
        previewView.layer.addSublayer(previewLayer!)
        
        previewLayer!.videoGravity = AVLayerVideoGravityResizeAspectFill
        cameraSettings.goCamera()
        if Display.typeIsLike == .iphoneX {
            topBar.frame=CGRect(x:0, y: 0, width:topBar.frame.width, height:topBar.frame.height+15)
        }
    }
    public func InitPlugin(parent:UITabBarController,marco:String){
        //print(listProducts)
        
        let storyboard = UIStoryboard(name: "JACSCamera", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "JACSCamera") as! UINavigationController
        
        selectedMarco = marco
        
        controller.modalPresentationStyle = .fullScreen
        controller.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        controller.providesPresentationContextTransitionStyle = true
        controller.definesPresentationContext = true
        parent.present(controller, animated: true, completion: nil)
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //startCamera(isFront:cambioDecamara)
       // CameraSettings.startCamera(isFront:false,previewView)
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        previewLayer!.frame = previewView.bounds
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func changeCamera(_ sender: Any) {
        //self.performSegue(withIdentifier: "seg_gotofront", sender: self)
       performSegue(withIdentifier: "seg_backtofront", sender: self)
    }
    
    @IBAction func didTakePhoto(_ sender: Any) {
        cameraSettings.takePhoto(selectedMarco:selectedMarco,area:marco, rotate:false, callback: { result in
            self.image=result
            self.performSegue(withIdentifier: "seg_backtosave", sender: self)
        })
    }
    
    func takeScreenshot(view: UIView) -> UIImageView {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        UIImageWriteToSavedPhotosAlbum(image!, nil, nil, nil)
        
        return UIImageView(image: image)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filtros.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath as IndexPath) as! CellGenerica
        cell.imageMarco.image = UIImage(named:filtros[indexPath.row])
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        marco.image=UIImage(named:filtros[indexPath.row])
        selectedMarco=filtros[indexPath.row];
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: 90, height: 45);
    }

    /**/
     // MARK: - Navigation
     
    @IBAction func goToGallery(_ sender: UIButton) {
        performSegue(withIdentifier: "seg_backtogallery", sender: self)
    }
    func imagePickerController(_ picker: UIImagePickerController,didFinishPickingMediaWithInfo info: [String : Any])
    {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //1
        captureImageView.contentMode = .scaleAspectFit //2
        captureImageView.image = chosenImage //3
        dismiss(animated:true, completion: nil) //4
    }

    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "seg_backtofront") {
            let vc = segue.destination as! FrontCameraController
            vc.selectedMarco = selectedMarco
        }
        if (segue.identifier == "seg_backtogallery") {
            let vc = segue.destination as! GalleryController
            vc.selectedMarco = selectedMarco
        }
        if(segue.identifier == "seg_backtosave") {
            let vc = segue.destination as! SaveController
            FIRAnalytics.logEvent(withName: selectedMarco,parameters: nil)
            vc.selectedMarco = selectedMarco
            vc.fromTo="seg_backtoBack"
            vc.imagen=image
        }
        
     }
    
}


