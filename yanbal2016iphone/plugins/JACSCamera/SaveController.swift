//
//  CameraViewController.swift
//  yanbal2016ipad
//
//  Created by Jeisson González on 27/02/18.
//  Copyright © 2018 wigilabs. All rights reserved.
//

import UIKit
import AVFoundation
@available(iOS 8.0, *)
class SaveController: UIViewController{
    var selectedMarco:String = ""
    var imagen=UIImage();
    var fromTo=""
     @IBOutlet var marco: UIImageView!
    @IBOutlet var captureImageView: UIImageView!
    var imagePicker = UIImagePickerController()
    @IBOutlet var screenShotArea: UIView!
    @IBOutlet var loading: UIImageView!
    let cameraSettings=CameraSettings()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
    }
    func initView(){
     captureImageView.image=imagen
     marco.image=UIImage(named:selectedMarco)
        imagen=cameraSettings.takeScreenshot(view: screenShotArea).image!
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //startCamera(isFront:cambioDecamara)
        // CameraSettings.startCamera(isFront:false,previewView)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func changeCamera(_ sender: Any) {
        performSegue(withIdentifier:"seg_backtoBack", sender:self)
        
    }
   
    // make a cell for each cell index path
    
    @IBAction func saveImage(_ sender: UIButton) {
        UIImageWriteToSavedPhotosAlbum(imagen, nil, nil, nil)
        let alertController = UIAlertController(title: "Yanbal", message: "Tu foto ha sido guardada en el carrete", preferredStyle: .alert)
        
        let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(defaultAction)
        
        present(alertController, animated: true, completion: nil)
    }
    
   
    @IBAction func share(_ sender: Any) {
        let textToShare = "#SoySmartSoyYanbal"
        
        if let myWebsite = NSURL(string: "") {
            
            let objectsToShare = [imagen] as [Any]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            activityVC.setValue("App Name", forKey: "subject")
            
            
            
            self.present(activityVC, animated: true, completion: nil)
        }
    }
    @IBAction func close(_ sender: UIButton) {
        
        performSegue(withIdentifier:fromTo , sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "seg_backtoBack") {
            let vc = segue.destination as! BackCameraController
            vc.selectedMarco = selectedMarco
        }
        if (segue.identifier == "seg_backtofront") {
            let vc = segue.destination as! FrontCameraController
            vc.selectedMarco = selectedMarco
        }
        if (segue.identifier == "seg_backtogallery") {
            let vc = segue.destination as! GalleryController
            vc.selectedMarco = selectedMarco
        }
        
    }
    
}




