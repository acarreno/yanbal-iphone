//
//  CameraViewController.swift
//  yanbal2016ipad
//
//  Created by Jeisson González on 27/02/18.
//  Copyright © 2018 wigilabs. All rights reserved.
//

import UIKit
import AVFoundation
import Firebase

@available(iOS 8.0, *)
class FrontCameraController: UIViewController,  UICollectionViewDataSource, UICollectionViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    @IBOutlet var collection: UICollectionView!
    @IBOutlet var previewView: UIView!
    @IBOutlet var captureImageView: UIImageView!
    @IBOutlet var marco: UIImageView!
    var selectedMarco:String = ""
    var cambioDecamara = false
    var cameraSettings=CameraSettings()
    var imagePicker = UIImagePickerController()
    @IBOutlet var screenShotArea: UIView!
    @IBOutlet var topBar: UIView!
    var captureSession: AVCaptureSession?
    var stillImageOutput: AVCaptureStillImageOutput?
    var previewLayer: AVCaptureVideoPreviewLayer?
    var image=UIImage()
    let structura = Marcos()
    
    var filtros:[String]=[]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initView()
        
    }
    func initView(){
        filtros=structura.marcos
        
        let layout = collection.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: 40, height: 50)
        if(selectedMarco==""){
            selectedMarco=filtros[0]
        }
        marco.image=UIImage(named:selectedMarco)
        previewLayer=cameraSettings.startCamera(isFront: true)
        previewView.layer.addSublayer(previewLayer!)
        previewLayer!.videoGravity = AVLayerVideoGravityResizeAspectFill
        cameraSettings.goCamera()
        
        previewLayer!.frame = previewView.bounds
        if Display.typeIsLike == .iphoneX {
            topBar.frame=CGRect(x:0, y: 0, width:topBar.frame.width, height:topBar.frame.height+15)
        }
    }
   
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //startCamera(isFront:cambioDecamara)
        // CameraSettings.startCamera(isFront:false,previewView)
        
        //previewLayer.view.frame.size=previewView.frame.size
        //previewLayer!.frame = previewView.bounds
        //previewLayer?.frame.size=previewView.frame.size
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        //let porcentaje=screenWidth*0.4
       // previewView.frame = CGRect(x: 0, y:-60 , width: screenWidth, height:screenHeight)
        
       // previewLayer?.frame = CGRect(x: 0, y:0 , width: screenWidth+porcentaje, height:screenHeight+porcentaje)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func changeCamera(_ sender: Any) {
        //self.performSegue(withIdentifier: "seg_gotofront", sender: self)
        performSegue(withIdentifier: "seg_backtoBack", sender: self)
    }
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func didTakePhoto(_ sender: Any) {
        
        cameraSettings.takePhoto(selectedMarco:selectedMarco, area:marco,rotate:true, callback: { result in
            self.image=result
            self.performSegue(withIdentifier: "seg_backtosave", sender: self)
        })
    }
    
    func takeScreenshot(view: UIView) -> UIImageView {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        UIImageWriteToSavedPhotosAlbum(image!, nil, nil, nil)
        
        return UIImageView(image: image)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filtros.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath as IndexPath) as! CellGenerica
        cell.imageMarco.image = UIImage(named:filtros[indexPath.row])
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        marco.image=UIImage(named:filtros[indexPath.row])
        selectedMarco=filtros[indexPath.row];
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: 90, height: 45);
    }
    
    /**/
    // MARK: - Navigation
    
    @IBAction func goToGallery(_ sender: UIButton) {
        performSegue(withIdentifier: "seg_backtogallery", sender: self)
    }
    func imagePickerController(_ picker: UIImagePickerController,didFinishPickingMediaWithInfo info: [String : Any])
    {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //1
        captureImageView.contentMode = .scaleAspectFit //2
        captureImageView.image = chosenImage //3
        dismiss(animated:true, completion: nil) //4
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "seg_backtoBack") {
            let vc = segue.destination as! BackCameraController
            vc.selectedMarco = selectedMarco
        }
        if (segue.identifier == "seg_backtogallery") {
            let vc = segue.destination as! GalleryController
            vc.selectedMarco = selectedMarco
        }
        if(segue.identifier == "seg_backtosave") {
            let vc = segue.destination as! SaveController
            vc.selectedMarco = selectedMarco
            FIRAnalytics.logEvent(withName: selectedMarco,parameters: nil)
            vc.fromTo="seg_backtofront"
            vc.imagen=image
        }
        
    }
    
}


