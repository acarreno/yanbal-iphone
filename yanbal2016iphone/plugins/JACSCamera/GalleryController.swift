//
//  CameraViewController.swift
//  yanbal2016ipad
//
//  Created by Jeisson González on 27/02/18.
//  Copyright © 2018 wigilabs. All rights reserved.
//

import UIKit
import AVFoundation
@available(iOS 8.0, *)
class GalleryController: UIViewController,  UICollectionViewDataSource, UICollectionViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    var selectedMarco:String = ""
    @IBOutlet var collection: UICollectionView!
    @IBOutlet var previewView: UIView!
    @IBOutlet var captureImageView: UIImageView!
    
    @IBOutlet var marco: UIImageView!
    var configurationCamera=CameraSettings()
    @IBOutlet var screenShotArea: UIView!
     @IBOutlet var topBar: UIView!
    var imagePicker = UIImagePickerController()
    let structura = Marcos()
    var  image=UIImage()
    var filtros:[String]=[]
    
    @IBOutlet var loading: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initView()
        let screenSize = UIScreen.main.bounds
        let screenWidth = screenSize.width
        let screenHeight = screenSize.height
        
        //previewView.frame = CGRect(x: 0, y:-60 , width: screenWidth, height:screenHeight-60)
    }
    func initView(){
        goToGallery()
        
        filtros=structura.marcos
        marco.image=UIImage(named:selectedMarco)
        let layout = collection.collectionViewLayout as! UICollectionViewFlowLayout
        layout.itemSize = CGSize(width: 40, height: 50)
        if Display.typeIsLike == .iphoneX {
            topBar.frame=CGRect(x:0, y: 0, width:topBar.frame.width, height:topBar.frame.height+15)
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        //startCamera(isFront:cambioDecamara)
        // CameraSettings.startCamera(isFront:false,previewView)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
        
    }
    @IBAction func back(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func changeCamera(_ sender: Any) {
        performSegue(withIdentifier:"seg_backtoBack", sender:self)
        
    }
    
    @IBAction func didTakePhoto(_ sender: Any) {
        image=configurationCamera.buildImage(bottomImage: captureImageView.image!, imageName: selectedMarco, screenShotArea:marco)
        performSegue(withIdentifier:"seg_backtosave", sender: self)
    }
    
    func takeScreenshot(view: UIView) -> UIImageView {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
        
        return UIImageView(image: image)
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filtros.count
    }
    
    // make a cell for each cell index path
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // get a reference to our storyboard cell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionCell", for: indexPath as IndexPath) as! CellGenerica
        cell.imageMarco.image = UIImage(named:filtros[indexPath.row])
        
        return cell
    }
    
    // MARK: - UICollectionViewDelegate protocol
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        // handle tap events
        marco.image=UIImage(named:filtros[indexPath.row])
        selectedMarco=filtros[indexPath.row];
    }
    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
        return CGSize(width: 90, height: 45);
    }
    func goToGallery() {
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum){
            print("Button capture")
            
            imagePicker.delegate = self
            imagePicker.sourceType = .savedPhotosAlbum;
            imagePicker.allowsEditing = false
            //imagePicker.interfaceOrientation = .landscapeRight
            self.present(imagePicker, animated: true, completion: nil)
            
        }
    }
    
    
    @IBAction func openGallery(_ sender: UIButton) {
        goToGallery()
    }
    func imagePickerController(_ picker: UIImagePickerController,didFinishPickingMediaWithInfo info: [String : Any])
    {
        let chosenImage = info[UIImagePickerControllerOriginalImage] as! UIImage //1
        captureImageView.contentMode = .scaleAspectFit //2
        captureImageView.image = chosenImage //3
        dismiss(animated:true, completion: nil) //4
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if (segue.identifier == "seg_backtoBack") {
            let vc = segue.destination as! BackCameraController
            vc.selectedMarco = selectedMarco
           
        }
        if (segue.identifier == "seg_backtosave") {
            let vc = segue.destination as! SaveController
            vc.imagen = image
            vc.fromTo="seg_backtogallery"
            vc.selectedMarco=selectedMarco
        }
        
        
    }
}



