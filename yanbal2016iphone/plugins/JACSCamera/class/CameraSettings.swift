//
//  File.swift
//  yanbal2016ipad
//
//  Created by Jeisson González on 1/03/18.
//  Copyright © 2018 wigilabs. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
class CameraSettings {
    var captureSession: AVCaptureSession?
    var stillImageOutput: AVCaptureStillImageOutput?
    var previewLayer: AVCaptureVideoPreviewLayer?
    var existeCamara:Bool=false
    func takePhoto(selectedMarco:String,area:UIImageView,rotate:Bool,callback:@escaping (UIImage)->()) {
        if(existeCamara){
            if let videoConnection = stillImageOutput!.connection(withMediaType: AVMediaTypeVideo) {
                
                
                stillImageOutput?.captureStillImageAsynchronously(from: videoConnection, completionHandler: {(sampleBuffer, error) in
                    if (sampleBuffer != nil) {
                        let imageData = AVCaptureStillImageOutput.jpegStillImageNSDataRepresentation(sampleBuffer)
                        let dataProvider = CGDataProvider(data: imageData! as CFData)
                        let cgImageRef = CGImage(jpegDataProviderSource: dataProvider!, decode: nil, shouldInterpolate: true, intent: CGColorRenderingIntent.defaultIntent)
                        var image = UIImage()
                        if(rotate){
                            image = UIImage(cgImage: cgImageRef!, scale: 1.0, orientation:UIImageOrientation.leftMirrored )
                        }else{
                            image = UIImage(cgImage: cgImageRef!, scale: 1.0, orientation:UIImageOrientation.right  )
                        }
                        let definitiveImage=self.buildImage( bottomImage:image,imageName:selectedMarco,screenShotArea:area);
                        callback(definitiveImage)
                        
                    }
                })
            }
        }else{
            if #available(iOS 8.0, *) {
                let alertController = UIAlertController(title: "Yanbal", message: "En este momento no es posible acceder a tu camara", preferredStyle: .alert)
           
                let defaultAction = UIAlertAction(title: "OK", style: .default, handler: nil)
                 alertController.addAction(defaultAction)
            } else {
                // Fallback on earlier versions
            }
           
        }
       
        //captureImageView.image = takeScreenshot(view:screenShotArea).image
    }
    func startCamera(isFront:Bool)->AVCaptureVideoPreviewLayer{
        
        if(captureSession==nil){
            captureSession = AVCaptureSession()
            captureSession!.sessionPreset = AVCaptureSessionPresetPhoto
            
        }
        
        //var backCamera = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        //AVCaptureDevicePosition.front
        //var backCamera = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        
        //if(isFront){
        
        //}
        for i : AVCaptureDeviceInput in (self.captureSession?.inputs as! [AVCaptureDeviceInput]){
            self.captureSession?.removeInput(i)
        }
        var camera = getDevice(position:.back)
        if(isFront){
            camera = getDevice(position:.front)
        }
        var error: NSError?
        var input: AVCaptureDeviceInput!
        do {
            
            input = try AVCaptureDeviceInput(device: camera)
        } catch let error1 as NSError {
            error = error1
            input = nil
        }
        
        
        
        if error == nil && captureSession!.canAddInput(input) {
            captureSession!.addInput(input)
            existeCamara=true
            stillImageOutput = AVCaptureStillImageOutput()
            stillImageOutput!.outputSettings = [AVVideoCodecKey: AVVideoCodecJPEG]
            if captureSession!.canAddOutput(stillImageOutput) {
                captureSession!.addOutput(stillImageOutput)
                //if(previewLayer != nil){
                //previewLayer!.removeFromSuperlayer()
                //}
                
                previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
                //videoPreviewLayer!.videoGravity = AVLayerVideoGravityResizeAspect
                previewLayer!.videoGravity = AVLayerVideoGravityResizeAspect
                previewLayer!.connection?.videoOrientation = AVCaptureVideoOrientation.portrait
                
                
                //previewVieW.subviews.forEach { $0.removeFromSuperview() }
                //previewView.layer.sublayers?.forEach { $0.removeFromSuperlayer() }
                
                //userView.frame =  CGRect(x:0, y: 0, width:0, height:0)
                let screenSize = UIScreen.main.bounds
                let screenWidth = screenSize.width
                let screenHeight = screenSize.height
                previewLayer?.frame =  CGRect(x:0, y: 0, width:screenWidth, height:screenHeight)
                
                previewLayer?.backgroundColor =  UIColor.blue.cgColor
               
                return previewLayer!
                
                
            }
        }
        previewLayer=AVCaptureVideoPreviewLayer()
        return previewLayer!
       
    }
    func goCamera(){
        captureSession!.startRunning()
        
    }
    func getDevice(position: AVCaptureDevicePosition) -> AVCaptureDevice? {
        let devices: NSArray = AVCaptureDevice.devices()! as NSArray;
        for de in devices {
            let deviceConverted = de as! AVCaptureDevice
            if(deviceConverted.position == position){
                return deviceConverted
            }
        }
        return nil
    }
    func buildImage( bottomImage:UIImage,imageName:String, screenShotArea:UIImageView)->UIImage{
        let size = CGSize(width:screenShotArea.frame.height, height:screenShotArea.frame.width)
       
        
        let areaSize = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        bottomImage.draw(in: areaSize)
         //let crop=bottomImage.othercrop( rect: areaSize)
        //topImage!.draw(in: CGRect(x: 0, y: 0, width: size.width, height: size.height))
        
        //let newImage:UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        //UIGraphicsEndImageContext()
        return bottomImage
    }
    
    func crop(image:UIImage, cropRect:CGRect) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(cropRect.size, false, image.scale)
        let origin = CGPoint(x: cropRect.origin.x * CGFloat(), y: cropRect.origin.y * CGFloat())
        image.draw(at: origin)
        let result = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        
        return result
    }
    func cropImage(image: UIImage, toRect: CGRect) -> UIImage? {
        // Cropping is available trhough CGGraphics
        let cgImage :CGImage! = image.cgImage
        let croppedCGImage: CGImage! = cgImage.cropping(to: toRect)
        let retImage=UIImage(cgImage: croppedCGImage)
        return retImage
    }
    func takeScreenshot(view: UIView) -> UIImageView {
        UIGraphicsBeginImageContext(view.frame.size)
        view.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        UIImageWriteToSavedPhotosAlbum(image!, nil, nil, nil)
        
        return UIImageView(image: image)
    }
    
}
