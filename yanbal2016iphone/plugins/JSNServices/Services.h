//
//  Usuario.h
//  walmart
//
//  Created by Jeisson González on 3/09/15.
//  Copyright (c) 2015 hidesoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFURLSessionManager.h"
#import "AFHTTPRequestOperationManager.h"
#import "SVProgressHUD.h"


@interface Services : NSObject


+(void)get:(NSString *)url delegate:(UIViewController *)delegate widthProgress:(BOOL)progress completion:(void (^)(BOOL finished,NSDictionary *res))completion;
+(void)getConcurso:(NSString *)url delegate:(UIViewController *)delegate widthProgress:(BOOL)progress completion:(void (^)(BOOL finished,NSDictionary *res))completion;


@end
