//
//  Usuario.m
//  walmart
//
//  Created by Jeisson González on 3/09/15.
//  Copyright (c) 2015 hidesoft. All rights reserved.
//

#import "Services.h"

@implementation Services



+(void)get:(NSString *)url delegate:(UIViewController *)delegate widthProgress:(BOOL)progress completion:(void (^)(BOOL finished,NSDictionary *res))completion{

    if (progress) {
        
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
        [SVProgressHUD showWithStatus:@"Cargando..."];
    }
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"data: %@", responseObject);
        
        if ([responseObject[@"status"] isEqualToString:@"ok"]) {
            
            [SVProgressHUD dismiss];
            completion(YES,responseObject);
            
        }else{
            
            if (progress) {
                
                NSString *mensaje=@"Error al conectarse con el el servidor.";
                
                if (responseObject[@"error"] && [responseObject[@"error"] isEqualToString:@"Invalid username/email and/or password."]) {
                    mensaje=@"Código incorrecto, intentelo de nuevo.";
                }
                
                [SVProgressHUD dismiss];
                
                UIAlertController *alert=[UIAlertController
                                          alertControllerWithTitle:@""
                                          message:mensaje
                                          preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yesButton = [UIAlertAction
                                            actionWithTitle:@"OK"
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action)
                                            {
                                                //Handel your yes please button action here
                                                [alert dismissViewControllerAnimated:YES completion:nil];
                                                completion(NO,[[NSDictionary alloc]init]);
                                                
                                            }];
                
                [alert addAction:yesButton];
                [delegate presentViewController:alert animated:YES completion:nil];
            }
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (progress) {
            [SVProgressHUD dismiss];
            
            
            UIAlertController *alert=[UIAlertController
                                      alertControllerWithTitle:@""
                                      message:@"Error al conectarse con el el servidor."
                                      preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"Reintentar"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                                            //Handel your yes please button action here
                                            [alert dismissViewControllerAnimated:YES completion:nil];
                                            [self get:url delegate:delegate widthProgress:progress completion:^(BOOL finished, NSDictionary *res) {
                                                completion(finished,res);
                                            }];
                                            
                                        }];
            
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:@"Cancelar"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           //Handel your yes please button action here
                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                           completion(NO,[[NSDictionary alloc]init]);
                                           
                                       }];
            
            [alert addAction:yesButton];
            [alert addAction:noButton];
            [delegate presentViewController:alert animated:YES completion:nil];
        }
        
    }];
    
}



+(void)getConcurso:(NSString *)url delegate:(UIViewController *)delegate widthProgress:(BOOL)progress completion:(void (^)(BOOL finished,NSDictionary *res))completion{
    
    if (progress) {
        
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
        [SVProgressHUD showWithStatus:@"Cargando..."];
    }
    
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:url parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        //NSLog(@"data: %@", responseObject);
        
        if ([responseObject[@"success"] intValue]==1) {
            
            [SVProgressHUD dismiss];
            completion(YES,responseObject);
            
        }else{
            
            if (progress) {
                
                NSString *mensaje=@"Error al conectarse con el el servidor.";
                
                if (responseObject[@"error"] && [responseObject[@"error"] isEqualToString:@"Invalid username/email and/or password."]) {
                    mensaje=@"Código incorrecto, intentelo de nuevo.";
                }
                
                [SVProgressHUD dismiss];
                
                UIAlertController *alert=[UIAlertController
                                          alertControllerWithTitle:@""
                                          message:mensaje
                                          preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* yesButton = [UIAlertAction
                                            actionWithTitle:@"OK"
                                            style:UIAlertActionStyleDefault
                                            handler:^(UIAlertAction * action)
                                            {
                                                //Handel your yes please button action here
                                                [alert dismissViewControllerAnimated:YES completion:nil];
                                                completion(NO,[[NSDictionary alloc]init]);
                                                
                                            }];
                
                [alert addAction:yesButton];
                [delegate presentViewController:alert animated:YES completion:nil];
            }
            
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        if (progress) {
            [SVProgressHUD dismiss];
            
            
            UIAlertController *alert=[UIAlertController
                                      alertControllerWithTitle:@""
                                      message:@"Error al conectarse con el el servidor."
                                      preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:@"Reintentar"
                                        style:UIAlertActionStyleDefault
                                        handler:^(UIAlertAction * action)
                                        {
                                            //Handel your yes please button action here
                                            [alert dismissViewControllerAnimated:YES completion:nil];
                                            [self get:url delegate:delegate widthProgress:progress completion:^(BOOL finished, NSDictionary *res) {
                                                completion(finished,res);
                                            }];
                                            
                                        }];
            
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:@"Cancelar"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action)
                                       {
                                           //Handel your yes please button action here
                                           [alert dismissViewControllerAnimated:YES completion:nil];
                                           completion(NO,[[NSDictionary alloc]init]);
                                           
                                       }];
            
            [alert addAction:yesButton];
            [alert addAction:noButton];
            [delegate presentViewController:alert animated:YES completion:nil];
        }
        
    }];
    
}


@end
